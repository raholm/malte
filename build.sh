#!/bin/bash

SCRIPT_PATH=`dirname $0 | xargs readlink -f`
BUILD_PATH=${SCRIPT_PATH}/build

if [ ! -d ${BUILD_PATH} ]; then
    echo "Creating ${BUILD_PATH}..."
    mkdir -p ${BUILD_PATH}
fi

pushd ${BUILD_PATH} > /dev/null

cmake ..
make -j12

popd > /dev/null
