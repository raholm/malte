cmake_minimum_required(VERSION 3.0.0)

project(malte CXX)

set(${PROJECT_NAME}_VERSION_MAJOR 0)
set(${PROJECT_NAME}_VERSION_MINOR 1)

# Compiler flags
set(DEFAULT_COMPILER_FLAGS
  -std=c++17
  -Wall
  -Wpedantic
  )

# Linker flags
set(DEFAULT_LINKER_FLAGS
  -lOpenCL
  -lglfw
  -lGL
  -ldl
  )

# CMake variables
set(CMAKE_BINARY_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/build)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIRECTORY}/lib)

# Variables
file(GLOB
  THIRDPARTY_DIR
  ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty)

# Include directories
include_directories(include)
include_directories(thirdparty)
include_directories(thirdparty/glm)

# Subdirectories
add_subdirectory(src)
