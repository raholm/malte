# Malte

A boids simulation based on the work of Craig Reynolds from 1986.
We are performing the simulation in OpenCL and rendering in OpenGL using shared buffers.

# Result

![Velocity](resources/videos/velocity.mp4)
![White](resources/videos/white.mp4)
