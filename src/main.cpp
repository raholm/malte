#include <random>
#include <vector>
#include <filesystem>
#include <fstream>
#include <sstream>

#include <malte/core/debug/log.h>
#include <malte/core/window.h>
#include <malte/core/math/trigonometry.h>
#include <malte/core/math.h>
#include <malte/core/random/uniform_sampler.h>
#include <malte/opengl/opengl.h>
#include <malte/opencl/opencl.h>

using namespace malte;

#define GL_ASSERT                                             \
  do {                                                        \
    auto gl_error = glGetError();                             \
    if (gl_error != GL_NO_ERROR) {                            \
      LogError("error: " << gl_error); fflush(stdout); \
      assert(false);                                          \
    }                                                         \
  } while (false)


const std::array<float, 24> cube_vertices = {
  -0.5f, -0.5f,  0.5f, // 2 <- 0
  0.5f, -0.5f,  0.5f, // 4 <- 1
  -0.5f,  0.5f,  0.5f, // 1 <- 2
  0.5f,  0.5f,  0.5f, // 3 <- 3
  -0.5f, -0.5f, -0.5f, // 6 <- 4
  0.5f, -0.5f, -0.5f, // 8 <- 5
  -0.5f,  0.5f, -0.5f, // 5 <- 6
  0.5f,  0.5f, -0.5f  // 7 <- 7
};

const std::array<u32, 24> cube_line_indices = {
  // Front
  2, 3,
  0, 1,
  0, 2,
  1, 3,

  // Back
  6, 7,
  4, 5,
  6, 4,
  7, 5,

  // Left
  2, 6,
  0, 4,

  // Right
  3, 7,
  1, 5,
};

template<typename T>
inline std::size_t get_data_size(const std::vector<T>& data)
{
  return data.size() * sizeof(T);
}

template<typename T>
inline void* get_data_ptr(const std::vector<T>& data)
{
  return (void*) data.data();
}

struct BoidSystem final
{
  u32 size;
  std::vector<glm::vec3> position_list;
  std::vector<glm::vec3> velocity_list;
};

struct Camera final
{
  glm::vec3 position;
  glm::vec3 target;
  glm::vec3 world_up;

  f32 field_of_view;
  f32 aspect_ratio;
  f32 near;
  f32 far;
};

struct ClSession
{
  ClPlatform platform;
  ClDevice device;
  ClContext context;
  ClCommandQueue command_queue;
  ClKernelSourceManager kernel_source_manager;

  bool is_null() const { return platform.is_null() || device.is_null() || context.is_null() || command_queue.is_null(); }
};

struct Vertex
{
  glm::vec3 position;
};

struct Triangle
{
  u32 v1 { 0 };
  u32 v2 { 0 };
  u32 v3 { 0 };
};

struct Mesh
{
  std::vector<Vertex> vertex_list;
  std::vector<Triangle> triangle_list;
};

Mesh ReadMesh()
{
  const char* project_path = getenv("MALTE_HOME");

  if (!project_path)
  {
    LogError("MALTE_HOME is not set!");
    exit(1);
  }

  std::filesystem::path file_path = project_path;
  file_path /= "resources";
  file_path /= "mesh";
  file_path /= "mesh.obj";

  std::ifstream infile { file_path };

  if (!infile)
  {
    LogError("Failed to read: " << file_path);
    exit(1);
  }

  Mesh result;
  std::string line;

  while (std::getline(infile, line))
  {
    std::istringstream iss { line };

    char type;
    iss >> type;

    if (type != 'v' &&
        type != 'f')
      continue;

    if (type == 'v')
    {
      Vertex vertex;
      iss >> vertex.position.x >> vertex.position.y >> vertex.position.z;

      result.vertex_list.push_back(vertex);
    }
    else
    {
      Triangle triangle;
      iss >> triangle.v1 >> triangle.v2 >> triangle.v3;

      triangle.v1--;
      triangle.v2--;
      triangle.v3--;

      result.triangle_list.push_back(triangle);
    }
  }

  LogInfo("Read: " << result.vertex_list.size() << " vertices, " << result.triangle_list.size() << " triangles");
  return result;
}

ClSession create_cl_session()
{
  ClSession session;
  session.platform = get_cl_platform("nvidia", true);

  if (session.platform.is_null())
    return session;

  session.device = session.platform.get_opengl_device();

  if (session.device.is_null())
    return session;

  session.context = ClContext(session.platform, session.device, true);

  if (session.context.is_null())
    return session;

  session.command_queue = ClCommandQueue(session.device, session.context);
  return session;
}

int main(int argc, char** argv)
{
  LogInfo("Welcome to malte");

  malte::Window window(1920, 1080, "malte");

  if (window.is_null())
    return 1;

  ClSession cl_session = create_cl_session();

  if (cl_session.is_null())
    return 1;

  LogInfo("Device:\n" << cl_session.device.get_properties().to_string());

  const Mesh mesh = ReadMesh();

  constexpr u32 boid_count = 1 << 7;

  UniformSampler sampler { 0 };
  // UniformSampler sampler { 1337 };

  BoidSystem boid_system;

  const f32 min_x = -static_cast<f32>(window.get_width()) / 2.0f;
  const f32 min_y = -static_cast<f32>(window.get_height()) / 2.0f;
  const f32 min_z = 0.0f;

  const f32 max_x = static_cast<f32>(window.get_width()) / 2.0f;
  const f32 max_y = static_cast<f32>(window.get_height()) / 2.0f;
  const f32 max_z = 320.0f;

  // Generate initial positions
  {
    boid_system.size = boid_count;
    boid_system.position_list.reserve(boid_count);
    boid_system.velocity_list.reserve(boid_count);

    for (u32 index = 0; index < boid_count; ++index)
    {
      glm::vec3 position;
      position.x = sampler.next(min_x, max_x);
      position.y = sampler.next(min_y, max_y);
      position.z = sampler.next(min_z, max_z);

      boid_system.position_list.push_back(position);
    }

    for (u32 index = 0; index < boid_count; ++index)
    {
      glm::vec3 velocity;
      velocity.x = sampler.next(-0.2f, 0.2f);
      velocity.y = sampler.next(-0.2f, 0.2f);
      velocity.z = sampler.next(-0.2f, 0.2f);

      velocity.x = 0.0f;
      velocity.y = 0.0f;
      velocity.z = 0.0f;

      boid_system.velocity_list.push_back(velocity);
    }
  }

  GLuint mesh_vertex_array_object;
  glGenVertexArrays(1, &mesh_vertex_array_object);
  GL_ASSERT;
  glBindVertexArray(mesh_vertex_array_object);
  GL_ASSERT;

  GLuint mesh_vertex_buffer_object;
  glGenBuffers(1, &mesh_vertex_buffer_object);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, mesh_vertex_buffer_object);
  GL_ASSERT;
  glBufferData(GL_ARRAY_BUFFER, get_data_size(mesh.vertex_list), get_data_ptr(mesh.vertex_list), GL_STATIC_DRAW);
  GL_ASSERT;
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*) 0);
  GL_ASSERT;
  glEnableVertexAttribArray(0);
  GL_ASSERT;
  glVertexAttribDivisor(0, 0);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  GL_ASSERT;

  GLuint mesh_instance_position_buffer_object;
  glGenBuffers(1, &mesh_instance_position_buffer_object);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, mesh_instance_position_buffer_object);
  GL_ASSERT;
  glBufferData(GL_ARRAY_BUFFER, get_data_size(boid_system.position_list), get_data_ptr(boid_system.position_list), GL_STATIC_DRAW);
  GL_ASSERT;
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*) 0);
  GL_ASSERT;
  glEnableVertexAttribArray(1);
  GL_ASSERT;
  glVertexAttribDivisor(1, 1);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  GL_ASSERT;

  GLuint mesh_instance_velocity_buffer_object;
  glGenBuffers(1, &mesh_instance_velocity_buffer_object);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, mesh_instance_velocity_buffer_object);
  GL_ASSERT;
  glBufferData(GL_ARRAY_BUFFER, get_data_size(boid_system.velocity_list), get_data_ptr(boid_system.velocity_list), GL_STATIC_DRAW);
  GL_ASSERT;
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*) 0);
  GL_ASSERT;
  glEnableVertexAttribArray(2);
  GL_ASSERT;
  glVertexAttribDivisor(2, 1);
  GL_ASSERT;
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  GL_ASSERT;

  GLuint mesh_index_buffer_object;
  glGenBuffers(1, &mesh_index_buffer_object);
  GL_ASSERT;
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_index_buffer_object);
  GL_ASSERT;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, get_data_size(mesh.triangle_list), get_data_ptr(mesh.triangle_list), GL_STATIC_DRAW);
  GL_ASSERT;
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  GL_ASSERT;

  glBindVertexArray(0);
  GL_ASSERT;

  // Generate objects
  GLuint cube_vertex_array_object;
  GLuint cube_vertex_buffer_object;
  GLuint cube_element_array_object;

  glGenVertexArrays(1, &cube_vertex_array_object);
  GL_ASSERT;
  glGenBuffers(1, &cube_vertex_buffer_object);
  GL_ASSERT;
  glGenBuffers(1, &cube_element_array_object);
  GL_ASSERT;

  // VAO
  glBindVertexArray(cube_vertex_array_object);
  GL_ASSERT;

  // VBO
  glBindBuffer(GL_ARRAY_BUFFER, cube_vertex_buffer_object);
  GL_ASSERT;

  glBufferData(GL_ARRAY_BUFFER,
               cube_vertices.size() * sizeof(cube_vertices[0]),
               cube_vertices.data(),
               GL_STATIC_DRAW);
  GL_ASSERT;
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(cube_vertices[0]), (void*) 0);
  GL_ASSERT;
  glEnableVertexAttribArray(0);
  GL_ASSERT;

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  GL_ASSERT;

  // EAO
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube_element_array_object);
  GL_ASSERT;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               cube_line_indices.size() * sizeof(cube_line_indices[0]),
               cube_line_indices.data(),
               GL_STATIC_DRAW);
  GL_ASSERT;

  glBindVertexArray(0);
  GL_ASSERT;

  Camera camera;
  camera.position = { -460.0f, 250.0f, -1000.0f };
  camera.target = { 0.0f, 0.0, 0.0f };
  camera.world_up = { 0.0f, 1.0f, 0.0f };
  camera.field_of_view = glm::radians(70.0f);
  camera.aspect_ratio = static_cast<f32>(window.get_width()) / static_cast<f32>(window.get_height());
  camera.near = 0.01f;
  camera.far = 100000.0f;

  GlShader boid_vertex_shader(GlShader::Type::Vertex, "boid");
  GlShader boid_fragment_shader(GlShader::Type::Fragment, "boid");

  GlShaderProgram boid_shader_program;
  boid_shader_program.attach(boid_vertex_shader);
  boid_shader_program.attach(boid_fragment_shader);
  boid_shader_program.link();
  boid_vertex_shader.destroy();
  boid_fragment_shader.destroy();

  GlShader cube_vertex_shader(GlShader::Type::Vertex, "cube");
  GlShader cube_fragment_shader(GlShader::Type::Fragment, "cube");

  GlShaderProgram cube_shader_program;
  cube_shader_program.attach(cube_vertex_shader);
  cube_shader_program.attach(cube_fragment_shader);
  cube_shader_program.link();
  cube_vertex_shader.destroy();
  cube_fragment_shader.destroy();

  ClBuffer device_position_buffer(ClBuffer::Mode::ReadWrite, mesh_instance_position_buffer_object, cl_session.context);
  ClBuffer device_velocity_buffer(ClBuffer::Mode::ReadWrite, mesh_instance_velocity_buffer_object, cl_session.context);

  cl_session.kernel_source_manager.add_kernel("boid", "boid");
  ClProgram kernel_program(cl_session.kernel_source_manager.get_source_code("boid"),
                           cl_session.context);
  kernel_program.build(cl_session.device, "-w -Werror -cl-std=CL3.0");

  ClKernel kernel(kernel_program, "UpdateBoid");
  kernel.set_arg(0, device_position_buffer);
  kernel.set_arg(1, device_velocity_buffer);
  kernel.set_arg(2, boid_system.size);

  ClNDRange global_work_size(boid_system.size);
  ClNDRange local_work_size(128);

  glEnable(GL_DEPTH_TEST);
  glViewport(0, 0, window.get_width(), window.get_height());

  while (window.is_open())
  {
    window.poll_events();

    // Run kernel
    glFinish();

    cl_session.command_queue.acquire_buffer(device_position_buffer);
    cl_session.command_queue.acquire_buffer(device_velocity_buffer);
    cl_session.command_queue.run_kernel(kernel,
                                        global_work_size,
                                        local_work_size);
    cl_session.command_queue.release_buffer(device_position_buffer);
    cl_session.command_queue.release_buffer(device_velocity_buffer);
    cl_session.command_queue.finish();

    // Render
    glClearColor(0.0f, 0.2f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Boids
    {
      boid_shader_program.use();
      GL_ASSERT;

      boid_shader_program.set("in_view",
                              glm::lookAt(camera.position,
                                          glm::normalize(camera.position - camera.target),
                                          camera.world_up));
      GL_ASSERT;
      boid_shader_program.set("in_perspective",
                              glm::perspective(camera.field_of_view,
                                               camera.aspect_ratio,
                                               camera.near,
                                               camera.far));
      GL_ASSERT;

      glBindVertexArray(mesh_vertex_array_object);
      GL_ASSERT;
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_index_buffer_object);
      GL_ASSERT;
      glDrawElementsInstanced(GL_TRIANGLES, mesh.triangle_list.size() * 3, GL_UNSIGNED_INT, 0, boid_system.size);
      GL_ASSERT;
    }

    // Cube
    {
      cube_shader_program.use();
      GL_ASSERT;

      cube_shader_program.set("in_view",
                              glm::lookAt(camera.position,
                                          glm::normalize(camera.position - camera.target),
                                          camera.world_up));
      GL_ASSERT;
      cube_shader_program.set("in_perspective",
                              glm::perspective(camera.field_of_view,
                                               camera.aspect_ratio,
                                               camera.near,
                                               camera.far));

      glm::mat4 transform = glm::mat4(1.0f);
      transform = glm::translate(transform, glm::vec3(0.0f, 0.0f, max_z / 2.0f));
      transform = glm::scale(transform, glm::vec3(2.0f * max_x, 2.0f * max_y, 2.0f * max_z));

      GL_ASSERT;
      cube_shader_program.set("in_transform", transform);
      GL_ASSERT;

      cube_shader_program.set("in_color", glm::vec4(0.1f, 0.3f, 0.3f, 1.0));
      GL_ASSERT;

      glBindVertexArray(cube_vertex_array_object);
      GL_ASSERT;
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube_element_array_object);
      GL_ASSERT;
      glDrawElements(GL_LINES, cube_line_indices.size(), GL_UNSIGNED_INT, 0);
      GL_ASSERT;
    }

    window.swap_buffers();
  }

  glDeleteBuffers(1, &mesh_vertex_buffer_object);
  glDeleteBuffers(1, &mesh_instance_position_buffer_object);
  glDeleteBuffers(1, &mesh_instance_velocity_buffer_object);
  glDeleteVertexArrays(1, &mesh_vertex_array_object);

  LogInfo("Successfully executed");
  return 0;
}
