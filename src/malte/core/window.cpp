#include <malte/core/window.h>

#include <malte/opengl/opengl_headers.h>

#include <malte/core/debug/log.h>
#include <malte/core/debug/assert.h>

namespace malte {

  namespace {

    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
    {
	    switch(action)
	    {
	    case GLFW_PRESS:
	    case GLFW_REPEAT:
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
        {
          glfwSetWindowShouldClose(window, GLFW_TRUE);
          break;
        }
        }
	    }
    }

    void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
    {
	    switch(action)
	    {
	    case GLFW_PRESS:
        switch(button)
        {
        case GLFW_MOUSE_BUTTON_LEFT:
        {
          double x_position, y_position;
          glfwGetCursorPos(window, &x_position, &y_position);
          LogDebug("Mouse Position: (" << x_position << ", " << y_position << ")");
          break;
        }
        }
        break;
	    }
    }

    void CursorMovementCallback(GLFWwindow* window, double x_position, double y_position)
    {
	    // LogDebug("(" << x_position << ", " << y_position << ")");
    }

  }

  Window::Window(const u32 width,
                 const u32 height,
                 const std::string& title)
    : m_width(width), m_height(height), m_title(title), m_handle(nullptr)
  {
    LogInternal("Creating window...");

    if (!glfwInit())
    {
	    LogError("Failed to initialize glfw");
	    return;
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    m_handle = glfwCreateWindow(m_width, m_height, m_title.c_str(), nullptr, nullptr);

    if (!m_handle)
    {
	    LogError("Failed to create glfw window");
	    return;
    }

    glfwGetFramebufferSize(m_handle, &m_screen_width, &m_screen_height);
    glfwSetKeyCallback(m_handle, key_callback);
    glfwSetCursorPosCallback(m_handle, CursorMovementCallback);
    glfwSetMouseButtonCallback(m_handle, mouse_button_callback);

    glfwMakeContextCurrent(m_handle);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
	    LogError("Failed to initialize glad");
	    m_handle = nullptr;
	    return;
    }
  }

  Window::Window(Window&& other)
  {
    std::swap(m_width, other.m_width);
    std::swap(m_height, other.m_height);
    std::swap(m_screen_width, other.m_screen_width);
    std::swap(m_screen_height, other.m_screen_height);
    std::swap(m_title, other.m_title);
    std::swap(m_handle, other.m_handle);
  }

  Window& Window::operator=(Window&& rhs)
  {
    std::swap(m_width, rhs.m_width);
    std::swap(m_height, rhs.m_height);
    std::swap(m_screen_width, rhs.m_screen_width);
    std::swap(m_screen_height, rhs.m_screen_height);
    std::swap(m_title, rhs.m_title);
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  Window::~Window()
  {
    LogInternal("Destroying window...");
    glfwDestroyWindow(m_handle);
    glfwTerminate();
  }

  void Window::swap_buffers() const
  {
    Assert(m_handle);

    glfwSwapBuffers(m_handle);
  }

  void Window::poll_events() const
  {
    glfwPollEvents();
  }

  void Window::set_title(const std::string& String)
  {
    glfwSetWindowTitle(m_handle, String.c_str());
  }

  u32 Window::get_width() const
  {
    return m_width;
  }

  u32 Window::get_height() const
  {
    return m_height;
  }

  u32 Window::get_screen_width() const
  {
    return static_cast<u32>(m_screen_width);
  }

  u32 Window::get_screen_height() const
  {
    return static_cast<u32>(m_screen_height);
  }

  std::string Window::get_title() const
  {
    return m_title;
  }

  GLFWwindow* Window::get_handle() const
  {
    return m_handle;
  }

  bool Window::is_null() const
  {
    return m_handle == nullptr;
  }

  bool Window::is_open() const
  {
    return !glfwWindowShouldClose(m_handle);
  }

}  // malte
