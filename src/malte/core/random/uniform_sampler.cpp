#include <malte/core/random/uniform_sampler.h>

#include <malte/core/debug/assert.h>

namespace malte {

  UniformSampler::UniformSampler()
    : m_random_generator(std::random_device{}())
  {}

  UniformSampler::UniformSampler(const SeedType seed)
    : m_random_generator(seed)
  {}

}  // malte
