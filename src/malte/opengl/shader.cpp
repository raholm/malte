#include <fstream>

#include <malte/opengl/shader.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  namespace  {

    std::string read_shader_source_from_file(const std::string& file_path)
    {
	    std::ifstream file(file_path);

	    if (!file)
	    {
        LogError("Could not open file: " << file_path);
        Assert(false);
	    }

	    std::string source((std::istreambuf_iterator<char>(file)),
                         std::istreambuf_iterator<char>());
	    return source;
    }

    void compile_shader(const GLuint shader, const std::string& file_path)
    {
	    glCompileShader(shader);

	    GLint success;
	    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	    if (!success)
	    {
        LogError("Shader: " << file_path);

        GLint info_log_length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
        std::string info_log;
        info_log.resize(static_cast<u32>(info_log_length));
        GLchar* gl_info_log = (GLchar*) info_log.c_str();


        glGetShaderInfoLog(shader, info_log_length, 0, gl_info_log);
        LogError("Failed to compile shader:\n" << info_log);
        Assert(false);
	    }
    }
  }

  GlShader::GlShader(const Type type,
                     const std::string& shader_name)
    : m_type(type)
  {
    std::string file_path = get_file_path(type, shader_name);
    std::string source = read_shader_source_from_file(file_path);
    GLchar* gl_source = (GLchar*) source.c_str();

    m_handle = glCreateShader(get_shader_type(m_type));

    if (!m_handle)
    {
	    LogError("Failed to create GlShader");
	    Assert(false);
    }

    glShaderSource(m_handle, 1, &gl_source, 0);
    compile_shader(m_handle, file_path);
  }

  GlShader::~GlShader()
  {
    destroy();
  }

  void GlShader::destroy()
  {
    if (m_handle)
    {
	    LogInternal("Destroing shader...");
	    glDeleteShader(m_handle);
	    m_handle = 0;
    }
  }

  GLuint GlShader::get_handle() const
  {
    return m_handle;
  }

  GlShader::Type GlShader::get_type() const
  {
    return m_type;
  }

  std::string GlShader::get_file_path(const Type type,
                                      const std::string& name)
  {
    std::string result = "./resources/shaders/" + name;

    switch(type)
    {
    case Type::Vertex:
    {
	    result += ".vert";
	    break;
    }
    case Type::Fragment:
    {
	    result += ".frag";
	    break;
    }
    }

    return result;
  }

  GLint GlShader::get_shader_type(const Type type) const
  {
    switch(type)
    {
    case Type::Vertex: return GL_VERTEX_SHADER;
    case Type::Fragment: return GL_FRAGMENT_SHADER;
    default: return -1;
    };
  }

}  // malte
