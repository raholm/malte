#include <malte/opengl/shader_program.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  GlShaderProgram::GlShaderProgram()
    : m_handle(glCreateProgram())
  {
    if (!m_handle)
    {
	    LogError("Failed to create GlShaderProgram");
	    Assert(false);
    }
  }

  GlShaderProgram::GlShaderProgram(GlShaderProgram&& other)
  {
    std::swap(m_handle, other.m_handle);
  }

  GlShaderProgram& GlShaderProgram::operator=(GlShaderProgram&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  GlShaderProgram::~GlShaderProgram()
  {
    if (m_handle)
    {
	    LogInternal("Destroying GlShaderProgram...");
	    glDeleteProgram(m_handle);
	    m_handle = 0;
    }
  }

  void GlShaderProgram::attach(const GlShader& shader)
  {
    Assert(m_handle);
    Assert(shader.get_handle());

    glAttachShader(m_handle, shader.get_handle());
  }

  void GlShaderProgram::link()
  {
    Assert(m_handle);

    glLinkProgram(m_handle);

    GLint success;
    glGetProgramiv(m_handle, GL_LINK_STATUS, &success);

    if (!success)
    {
	    GLint info_log_length;
	    glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &info_log_length);

	    std::string info_log;
	    info_log.resize(info_log_length);
	    GLchar* gl_info_log = (GLchar*) info_log.c_str();

	    glGetProgramInfoLog(m_handle, info_log_length, 0, gl_info_log);
	    LogError("Failed to link GlShaderProgram:\n" << info_log);
	    Assert(false);
    }
  }

  void GlShaderProgram::use()
  {
    Assert(m_handle);

    glUseProgram(m_handle);
  }

  void GlShaderProgram::set(const std::string& name, const bool value)
  {
    Assert(m_handle);

    glUniform1i(location(name), value);
  }

  void GlShaderProgram::set(const std::string& name, const f32 value)
  {
    Assert(m_handle);

    glUniform1f(location(name), value);
  }

  void GlShaderProgram::set(const std::string& name, const s32 value)
  {
    Assert(m_handle);

    glUniform1i(location(name), value);
  }

  void GlShaderProgram::set(const std::string& name, const u32 value)
  {
    Assert(m_handle);

    glUniform1ui(location(name), value);
  }


  void GlShaderProgram::set(const std::string& name, const glm::vec3& value)
  {
    Assert(m_handle);

    glUniform3f(location(name), value.x, value.y, value.z);
  }

  void GlShaderProgram::set(const std::string& name, const glm::vec4& value)
  {
    Assert(m_handle);

    glUniform4f(location(name), value.x, value.y, value.z, value.w);
  }

  void GlShaderProgram::set(const std::string& name, const glm::mat4& value)
  {
    Assert(m_handle);

    glUniformMatrix4fv(location(name), 1, GL_FALSE, glm::value_ptr(value));
  }

  GLint GlShaderProgram::location(const std::string& name)
  {
    GLint location = glGetUniformLocation(m_handle, name.c_str());
    Assert(location >= 0);
    return location;
  }

}  // malte
