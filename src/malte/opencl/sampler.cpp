#include <malte/opencl/sampler.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClSampler::ClSampler(const ClSamplerProperties& properties,
                       const ClContext& context)
    : m_properties(properties)
  {
    Assert(!context.is_null());

    const cl_sampler_properties sampler_properties[] = {
	    CL_SAMPLER_NORMALIZED_COORDS, m_properties.normalized_coordinates,
	    CL_SAMPLER_ADDRESSING_MODE, m_properties.addressing_mode,
	    CL_SAMPLER_FILTER_MODE, m_properties.filter_mode,
	    0
    };

    ClStatus status;
    m_handle = clCreateSamplerWithProperties(context.get_handle(),
                                             sampler_properties,
                                             status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClSampler: " << status);
	    m_handle = nullptr;
    }
  }

  ClSampler::ClSampler(ClSampler&& other)
  {
    std::swap(m_handle, other.m_handle);
  }

  ClSampler& ClSampler::operator=(ClSampler&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  ClSampler::~ClSampler()
  {
    destroy();
  }

  void ClSampler::destroy()
  {
    if (!is_null())
    {
	    LogInternal("Destroying ClSampler...");
	    ClStatus status;
	    status = clReleaseSampler(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClSampler: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClSampler::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClSampler::set_null()
  {
    m_handle = nullptr;
  }

  const ClSamplerProperties& ClSampler::get_properties() const
  {
    return m_properties;
  }

  cl_sampler ClSampler::get_handle() const
  {
    return m_handle;
  }

}  // malte
