#include <malte/opencl/platform.h>
#include <malte/opencl/status.h>

#include <algorithm>

#include <malte/core/debug/log.h>
#include <malte/opengl/opengl_headers.h>

namespace malte {

  ClPlatform::ClPlatform(const cl_platform_id handle)
    : m_handle(handle)
  {}

  std::vector<ClDevice> ClPlatform::get_devices(const cl_device_type type) const
  {
    LogInternal("Getting device id(s) for platform " << m_handle << "...");

    ClStatus status;
    cl_uint device_count = 0;
    std::vector<cl_device_id> device_ids;

    status = clGetDeviceIDs(m_handle,
                            type,
                            0,
                            nullptr,
                            &device_count);

    if (status.is_error())
    {
	    LogError("Failed to get the number of device ids: " << status);
	    return std::vector<ClDevice>();
    }

    if (device_count == 0)
    {
	    LogWarning("There are no devices of type: " << get_device_type_string(type));
	    return std::vector<ClDevice>();
    }

    device_ids.resize(device_count);
    status = clGetDeviceIDs(m_handle,
                            type,
                            device_count,
                            device_ids.data(),
                            nullptr);

    if (status.is_error())
    {
	    LogError("Failed to get the device ids: " << status);
	    return std::vector<ClDevice>();
    }

    LogInternal("Transforming platform id(s) to cl_platform object(s)");

    std::vector<ClDevice> output(device_ids.size());
    std::transform(device_ids.cbegin(),
                   device_ids.cend(),
                   output.begin(),
                   [](const cl_device_id id) {
                     return ClDevice(id);
                   });

    LogInternal("Successfully got " << output.size() << " device(s)");
    return output;
  }

  ClDevice ClPlatform::get_opengl_device() const
  {
    clGetGLContextInfoKHR_fn clGetGLContextInfoKHR =
	    (clGetGLContextInfoKHR_fn) clGetExtensionFunctionAddressForPlatform(m_handle, "clGetGLContextInfoKHR");

    const cl_context_properties properties[] = {
	    CL_CONTEXT_PLATFORM, (cl_context_properties) m_handle,
	    CL_GL_CONTEXT_KHR, (cl_context_properties) glXGetCurrentContext(),
	    CL_GLX_DISPLAY_KHR, (cl_context_properties) glXGetCurrentDisplay(),
	    0,
    };

    std::size_t size = 0;
    ClStatus status;
    status = clGetGLContextInfoKHR(properties, CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR, 0, nullptr, &size);

    if (status.is_error())
    {
	    LogError("Failed to get opengl device: " << status);
	    return ClDevice();
    }

    if (size == 0)
    {
	    LogError("Failed to get opengl device because a opengl context has not been created");
	    return ClDevice();
    }

    cl_device_id DeviceId = nullptr;
    status = clGetGLContextInfoKHR(properties, CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR, size, &DeviceId, nullptr);

    if (status.is_error())
    {
	    LogError("Failed to get opengl device: " << status);
	    return ClDevice();
    }

    return ClDevice(DeviceId);
  }

  bool ClPlatform::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClPlatform::set_null()
  {
    m_handle = nullptr;
  }

  cl_platform_id ClPlatform::get_handle() const
  {
    return m_handle;
  }

  ClPlatformProperties ClPlatform::get_properties() const
  {
    return ClPlatformProperties(*this);
  }

  std::string ClPlatform::get_device_type_string(const cl_device_type type) const
  {
    std::ostringstream oss;

    if (type & CL_DEVICE_TYPE_CPU)
	    oss << "CL_DEVICE_TYPE_CPU ";

    if (type & CL_DEVICE_TYPE_GPU)
	    oss << "CL_DEVICE_TYPE_GPU ";

    if (type & CL_DEVICE_TYPE_ACCELERATOR)
	    oss << "CL_DEVICE_TYPE_ACCELERATOR ";

    if (type & CL_DEVICE_TYPE_DEFAULT)
	    oss << "CL_DEVICE_TYPE_DEFAULT ";

    if (type & CL_DEVICE_TYPE_ALL)
	    oss << "CL_DEVICE_TYPE_ALL ";

    return oss.str();
  }

  ClPlatformProperties::ClPlatformProperties(const ClPlatform& platform)
  {
    name = get_property_string(platform.get_handle(), CL_PLATFORM_NAME);
    vendor = get_property_string(platform.get_handle(), CL_PLATFORM_VENDOR);
    profile = get_property_string(platform.get_handle(), CL_PLATFORM_PROFILE);
    version = get_property_string(platform.get_handle(), CL_PLATFORM_VERSION);
    extensions = get_property_string(platform.get_handle(), CL_PLATFORM_EXTENSIONS);
  }

  std::string ClPlatformProperties::to_string() const
  {
    std::ostringstream oss;

    if (name.has_value())
	    oss << "Platform: " << name.value() << "\n";

    if (vendor.has_value())
	    oss << "Vendor: " << vendor.value() << "\n";

    if (version.has_value())
	    oss << "Version: " << version.value() << "\n";

    if (profile.has_value())
	    oss << "Profile: " << profile.value() << "\n";

    if (extensions.has_value())
	    oss << "Extensions: " << extensions.value() << "\n";

    return oss.str();
  }

  std::optional<std::string> ClPlatformProperties::get_property_string(const cl_platform_id platform_id,
                                                                       const cl_platform_info property_id) const
  {
    LogInternal("Getting the platform property " << property_id_to_string(property_id) << "...");

    ClStatus status;
    std::size_t size;
    std::string property;

    status = clGetPlatformInfo(platform_id,
                               property_id,
                               0,
                               NULL,
                               &size);

    if (status.is_error())
    {
	    LogError("Failed to get the size of the property" << status);
	    return {};
    }

    property.resize(size);
    status = clGetPlatformInfo(platform_id,
                               property_id,
                               size,
                               (void*) property.c_str(),
                               NULL);

    if (status.is_error())
    {
	    LogError("Failed to get the property value: " << status);
	    return {};
    }

    LogInternal("Successfully got the property");
    return property;
  }

  std::string ClPlatformProperties::property_id_to_string(const cl_platform_info property_id) const
  {
    switch(property_id)
    {
    case 2304: return "CL_PLATFORM_PROFILE";
    case 2305: return "CL_PLATFORM_VERSION";
    case 2306: return "CL_PLATFORM_NAME";
    case 2307: return "CL_PLATFORM_VENDOR";
    case 2308: return "CL_PLATFORM_EXTENSIONS";
    default: return "Unknown OpenCL platform property";
    }
  }

  std::vector<ClPlatform> get_cl_platforms(const bool opengl_sharing)
  {
    LogInternal("Getting OpenCL platform(s)...");

    ClStatus status;
    cl_uint platform_count;
    std::vector<cl_platform_id> platform_ids;

    status = clGetPlatformIDs(0, nullptr, &platform_count);

    if (status.is_error() || platform_count <= 0)
    {
	    LogError("Failed to find any ClPlatforms: " << status);
	    return std::vector<ClPlatform>();
    }

    platform_ids.resize(platform_count);
    status = clGetPlatformIDs(platform_count, platform_ids.data(), nullptr);

    if (status.is_error() || platform_count <= 0)
    {
	    LogError("Failed to find any ClPlatforms: " << status);
	    platform_ids.clear();
	    return std::vector<ClPlatform>();
    }

    LogInternal("Transforming platform id(s) to cl_platform object(s)");

    std::vector<ClPlatform> output;
    output.reserve(platform_ids.size());

    for (cl_platform_id platform_id : platform_ids)
    {
	    ClPlatform platform(platform_id);

	    // Make sure to only return the platforms that support opengl sharing
	    // if we only want those
	    if (opengl_sharing)
	    {
        ClPlatformProperties properties = platform.get_properties();

        if (properties.extensions.has_value())
        {
          const std::string& extensions = properties.extensions.value();
          std::size_t position = extensions.find("cl_khr_gl_sharing");
          if (position != std::string::npos)
            output.emplace_back(platform);
        }
	    }
	    else
	    {
        output.emplace_back(platform);
	    }
    }

    LogInternal("Successfully got " << output.size() << " OpenCL platforms");

    for (const ClPlatform& platform : output)
	    LogInternal("\n" << ClPlatformProperties(platform).to_string());

    return output;
  }

  ClPlatform get_cl_platform(const std::string& vendor, const bool opengl_sharing)
  {
    std::vector<ClPlatform> platforms = get_cl_platforms(opengl_sharing);

    for (const ClPlatform& platform : platforms)
    {
	    ClPlatformProperties properties(platform);

	    if (!properties.name.has_value())
        continue;

	    std::string name = properties.name.value();
	    std::transform(name.begin(),
                     name.end(),
                     name.begin(),
                     [](const char ch) { return std::tolower(ch); });

	    if (name.find(vendor) != std::string::npos)
        return platform;
    }

    return ClPlatform();
  }

}  // malte
