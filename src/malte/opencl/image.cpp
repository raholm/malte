#include <malte/opencl/image.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClImage::ClImage(const cl_mem_flags flags,
                   const cl_image_desc description,
                   const cl_image_format format,
                   const ClContext& context)
    : m_description(description),
      m_format(format),
      m_flags(flags)
  {
    Assert(!context.is_null());

    ClStatus status;
    m_handle = clCreateImage(context.get_handle(),
                             m_flags,
                             &m_format,
                             &m_description,
                             nullptr,
                             status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClImage: " << status);
	    m_handle = nullptr;
    }
  }

  ClImage::~ClImage()
  {
    destroy();
  }

  void ClImage::destroy()
  {

    if (!is_null())
    {
	    LogInternal("Destroying ClImage...");
	    ClStatus status;
	    status = clReleaseMemObject(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClImage: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClImage::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClImage::set_null()
  {
    m_handle = nullptr;
  }

  cl_mem ClImage::get_handle() const
  {
    return m_handle;
  }

  cl_image_desc ClImage::get_description() const
  {
    return m_description;
  }

  cl_image_format ClImage::get_format() const
  {
    return m_format;
  }

  cl_mem_flags ClImage::get_flags() const
  {
    return m_flags;
  }

}  // malte
