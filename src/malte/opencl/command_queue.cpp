#include <malte/opencl/command_queue.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClCommandQueue::ClCommandQueue(const ClDevice& device,
                                 const ClContext& context)
  {
    Assert(!device.is_null());
    Assert(!context.is_null());

    ClStatus status;
    const cl_queue_properties* properties = nullptr;
    m_handle = clCreateCommandQueueWithProperties(context.get_handle(),
                                                  device.get_handle(),
                                                  properties,
                                                  status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClCommandQueue: " << status);
	    m_handle = nullptr;
    }
  }

  ClCommandQueue::ClCommandQueue(ClCommandQueue&& other)
  {
    std::swap(m_handle, other.m_handle);
  }

  ClCommandQueue& ClCommandQueue::operator=(ClCommandQueue&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  ClCommandQueue::~ClCommandQueue()
  {
    destroy();
  }

  void ClCommandQueue::destroy()
  {
    if (!is_null())
    {
	    LogInternal("Destroying ClCommandQueue...");
	    ClStatus status;
	    status = clReleaseCommandQueue(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClCommandQueue: " << status);
	    else
        m_handle = nullptr;
    }
  }

  ClStatus ClCommandQueue::write_buffer(const ClBuffer& buffer,
                                        const bool synchronize,
                                        const std::size_t offset,
                                        const std::size_t size,
                                        const void *const data)
  {
    Assert(!is_null());;

    ClStatus status;
    status = clEnqueueWriteBuffer(m_handle,
                                  buffer.get_handle(),
                                  synchronize,
                                  offset,
                                  size,
                                  data,
                                  0,
                                  nullptr,
                                  nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue write buffer: " << status);

    return status;
  }

  ClStatus ClCommandQueue::read_buffer(const ClBuffer& buffer,
                                       const bool synchronize,
                                       const std::size_t offset,
                                       const std::size_t size,
                                       void *const data)
  {
    Assert(!is_null());

    ClStatus status;
    status = clEnqueueReadBuffer(m_handle,
                                 buffer.get_handle(),
                                 synchronize,
                                 offset,
                                 size,
                                 data,
                                 0,
                                 nullptr,
                                 nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue read buffer: " << status);

    return status;
  }

  ClStatus ClCommandQueue::fill_buffer(const ClBuffer& buffer,
                                       const std::size_t pattern_size,
                                       const void *const pattern,
                                       const std::size_t offset,
                                       const std::size_t size)
  {
    Assert(!is_null());

    ClStatus status;
    status = clEnqueueFillBuffer(m_handle,
                                 buffer.get_handle(),
                                 pattern,
                                 pattern_size,
                                 offset,
                                 size,
                                 0,
                                 nullptr,
                                 nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue fill buffer: " << status);

    return status;
  }

  ClStatus ClCommandQueue::run_kernel(const ClKernel& kernel,
                                      const ClNDRange global_work_size,
                                      const ClNDRange local_work_size,
                                      const ClNDRange global_work_offset)
  {
    Assert(!is_null());

    ClStatus status;
    status = clEnqueueNDRangeKernel(m_handle,
                                    kernel.get_handle(),
                                    global_work_size.get_dim(),
                                    global_work_offset.get_data(),
                                    global_work_size.get_data(),
                                    local_work_size.get_data(),
                                    0,
                                    nullptr,
                                    nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue ndrange kernel: " << status);

    return status;
  }

  ClStatus ClCommandQueue::write_image(const ClImage& image,
                                       const bool synchronize,
                                       const ClNDRange origin,
                                       const ClNDRange region,
                                       void *const data,
                                       const std::size_t row_pitch,
                                       const std::size_t slice_pitch)
  {
    Assert(!is_null());

    ClStatus status;
    status = clEnqueueWriteImage(m_handle,
                                 image.get_handle(),
                                 synchronize,
                                 origin.get_data(),
                                 region.get_data(),
                                 row_pitch,
                                 slice_pitch,
                                 data,
                                 0,
                                 nullptr,
                                 nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue write image: " << status);

    return status;
  }


  ClStatus ClCommandQueue::read_image(const ClImage& image,
                                      const bool synchronize,
                                      const ClNDRange origin,
                                      const ClNDRange region,
                                      void *const data,
                                      const std::size_t row_pitch,
                                      const std::size_t slice_pitch)
  {
    Assert(!is_null());

    ClStatus status;
    status = clEnqueueReadImage(m_handle,
                                image.get_handle(),
                                synchronize,
                                origin.get_data(),
                                region.get_data(),
                                row_pitch,
                                slice_pitch,
                                data,
                                0,
                                nullptr,
                                nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue read image: " << status);

    return status;
  }

  ClStatus ClCommandQueue::acquire_buffer(const ClBuffer& buffer)
  {
    Assert(!is_null());

    cl_mem handle = buffer.get_handle();

    ClStatus status;
    status = clEnqueueAcquireGLObjects(m_handle,
                                       1,
                                       &handle,
                                       0,
                                       nullptr,
                                       nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue acquire gl objects: " << status);

    return status;
  }


  ClStatus ClCommandQueue::release_buffer(const ClBuffer& buffer)
  {
    Assert(!is_null());

    cl_mem handle = buffer.get_handle();

    ClStatus status;
    status = clEnqueueReleaseGLObjects(m_handle,
                                       1,
                                       &handle,
                                       0,
                                       nullptr,
                                       nullptr);

    if (status.is_error())
	    LogError("Failed to enqueue release gl objects: " << status);

    return status;
  }

  ClStatus ClCommandQueue::flush()
  {
    Assert(!is_null());

    ClStatus status;
    status = clFlush(m_handle);

    if (status.is_error())
	    LogError("Failed to flush ClCommandQueue: " << status);

    return status;
  }

  ClStatus ClCommandQueue::finish()
  {
    Assert(!is_null());

    ClStatus status;
    status = clFinish(m_handle);

    if (status.is_error())
	    LogError("Failed to finish ClCommandQueue: " << status);

    return status;
  }

  ClStatus ClCommandQueue::flush_and_finish()
  {
    Assert(!is_null());

    ClStatus status;
    status = flush();

    if (status.is_error())
	    return status;

    return finish();
  }

  bool ClCommandQueue::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClCommandQueue::set_null()
  {
    m_handle = nullptr;
  }

  cl_command_queue ClCommandQueue::get_handle() const
  {
    return m_handle;
  }

}  // malte
