#include <malte/opencl/device.h>

#include <malte/core/debug/log.h>

namespace malte {

  ClDevice::ClDevice(const cl_device_id handle)
    : m_handle(handle)
  {}

  bool ClDevice::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClDevice::set_null()
  {
    m_handle = nullptr;
  }

  cl_device_id ClDevice::get_handle() const
  {
    return m_handle;
  }

  ClDeviceProperties ClDevice::get_properties() const
  {
    return ClDeviceProperties(*this);
  }

  template<>
  std::optional<std::string> ClDeviceProperties::get_property_value(const cl_device_id device_id,
                                                                    const cl_device_info property_name) const
  {
    ClStatus status;
    std::size_t size;
    std::string output;

    status = clGetDeviceInfo(device_id,
                             property_name,
                             0,
                             nullptr,
                             &size);

    if (status.is_error())
    {
	    LogError("Failed to get property size: " << status);
	    return {};
    }

    output.resize(size);
    status = clGetDeviceInfo(device_id,
                             property_name,
                             size,
                             (void*) output.c_str(),
                             nullptr);

    if (status.is_error())
    {
	    LogError("Failed to get property: " << status);
	    return {};
    }

    return output;
  }

  ClDeviceProperties::ClDeviceProperties(const ClDevice& device)
  {
    auto device_handle = device.get_handle();

    Name = get_property_value<std::string>(device_handle, CL_DEVICE_NAME);
    Type = get_property_value<cl_device_type>(device_handle, CL_DEVICE_TYPE);
    Version = get_property_value<std::string>(device_handle, CL_DEVICE_VERSION);
    VendorId = get_property_value<cl_uint>(device_handle, CL_DEVICE_VENDOR_ID);
    Vendor = get_property_value<std::string>(device_handle, CL_DEVICE_VENDOR);
    Profile = get_property_value<std::string>(device_handle, CL_DEVICE_PROFILE);
    DriverVersion = get_property_value<std::string>(device_handle, CL_DRIVER_VERSION);
    PlatformId = get_property_value<cl_platform_id>(device_handle, CL_DEVICE_PLATFORM);
    extensions = get_property_value<std::string>(device_handle, CL_DEVICE_EXTENSIONS);

    MaxComputeUnits = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_COMPUTE_UNITS);
    MaxWorkItemDimensions = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
    MaxWorkItemSizes = get_property_value_vector<std::size_t>(device_handle, CL_DEVICE_MAX_WORK_ITEM_SIZES);
    MaxWorkGroupSize = get_property_value<std::size_t>(device_handle, CL_DEVICE_MAX_WORK_GROUP_SIZE);

    PreferredVectorWidthChar = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR);
    PreferredVectorWidthShort = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT);
    PreferredVectorWidthInt = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT);
    PreferredVectorWidthLong = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG);
    PreferredVectorWidthFloat = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT);
    PreferredVectorWidthDouble = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE);
    PreferredVectorWidthHalf = get_property_value<cl_uint>(device_handle, CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF);

    NativeVectorWidthChar = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR);
    NativeVectorWidthShort = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT);
    NativeVectorWidthInt = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_INT);
    NativeVectorWidthLong = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG);
    NativeVectorWidthFloat = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT);
    NativeVectorWidthDouble = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE);
    NativeVectorWidthHalf = get_property_value<cl_uint>(device_handle, CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF);

    MaxClockFrequency = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_CLOCK_FREQUENCY);
    AddressBits = get_property_value<cl_uint>(device_handle, CL_DEVICE_ADDRESS_BITS);

    MaxMemAllocSize = get_property_value<cl_ulong>(device_handle, CL_DEVICE_MAX_MEM_ALLOC_SIZE);
    MemBaseAddrAlign = get_property_value<cl_uint>(device_handle, CL_DEVICE_MEM_BASE_ADDR_ALIGN);
    MinDataTypeAlignSize = get_property_value<cl_uint>(device_handle, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE);
    EndianLittle = get_property_value<cl_bool>(device_handle, CL_DEVICE_ENDIAN_LITTLE);

    GlobalMemCacheType = get_property_value<cl_device_mem_cache_type>(device_handle, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE);
    GlobalMemCachelineSize = get_property_value<cl_uint>(device_handle, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE);
    GlobalMemCacheSize = get_property_value<cl_ulong>(device_handle, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE);
    GlobalMemSize = get_property_value<cl_ulong>(device_handle, CL_DEVICE_GLOBAL_MEM_SIZE);
    MaxConstantBufferSize = get_property_value<cl_ulong>(device_handle, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE);
    MaxConstantArgs = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_CONSTANT_ARGS);
    LocalMemType = get_property_value<cl_device_local_mem_type>(device_handle, CL_DEVICE_LOCAL_MEM_TYPE);
    LocalMemSize = get_property_value<cl_ulong>(device_handle, CL_DEVICE_LOCAL_MEM_SIZE);
    ErrorCorrectionSupport = get_property_value<cl_bool>(device_handle, CL_DEVICE_ERROR_CORRECTION_SUPPORT);
    HostUnifiedMemory = get_property_value<cl_bool>(device_handle, CL_DEVICE_HOST_UNIFIED_MEMORY);

    ImageSupport = get_property_value<cl_bool>(device_handle, CL_DEVICE_IMAGE_SUPPORT);
    MaxReadImageArgs = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_READ_IMAGE_ARGS);
    MaxWriteImageArgs = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_WRITE_IMAGE_ARGS);
    Image2dMaxWidth = get_property_value<std::size_t>(device_handle, CL_DEVICE_IMAGE2D_MAX_WIDTH);
    Image2dMaxHeight = get_property_value<std::size_t>(device_handle, CL_DEVICE_IMAGE2D_MAX_HEIGHT);
    Image3dMaxWidth = get_property_value<std::size_t>(device_handle, CL_DEVICE_IMAGE3D_MAX_WIDTH);
    Image3dMaxHeight = get_property_value<std::size_t>(device_handle, CL_DEVICE_IMAGE3D_MAX_HEIGHT);
    Image3dMaxDepth = get_property_value<std::size_t>(device_handle, CL_DEVICE_IMAGE3D_MAX_DEPTH);

    MaxSamplers = get_property_value<cl_uint>(device_handle, CL_DEVICE_MAX_SAMPLERS);
    MaxParameterSize = get_property_value<std::size_t>(device_handle, CL_DEVICE_MAX_PARAMETER_SIZE);
    SingleFpConfig = get_property_value<cl_device_fp_config>(device_handle, CL_DEVICE_SINGLE_FP_CONFIG);
    DoubleFpConfig = get_property_value<cl_device_fp_config>(device_handle, CL_DEVICE_DOUBLE_FP_CONFIG);
    HalfFpConfig = get_property_value<cl_device_fp_config>(device_handle, CL_DEVICE_HALF_FP_CONFIG);

    ProfilingTimerResolution = get_property_value<std::size_t>(device_handle, CL_DEVICE_PROFILING_TIMER_RESOLUTION);
    Available = get_property_value<cl_bool>(device_handle, CL_DEVICE_AVAILABLE);;
    CompilerAvailable = get_property_value<cl_bool>(device_handle, CL_DEVICE_COMPILER_AVAILABLE);
    ExecutionCapabilities = get_property_value<cl_device_exec_capabilities>(device_handle, CL_DEVICE_EXECUTION_CAPABILITIES);
    QueueProperties = get_property_value<cl_command_queue_properties>(device_handle, CL_DEVICE_QUEUE_PROPERTIES);
  }

  std::string ClDeviceProperties::to_string() const
  {
    std::ostringstream oss;

    if (Name.has_value()) oss << "Name: " << Name.value() << "\n";
    if (Type.has_value()) oss << "Type: " << device_type_to_string(Type.value()) << "\n";
    if (Version.has_value()) oss << "Version: " << Version.value() << "\n";
    if (VendorId.has_value()) oss << "Vendor Id: " << VendorId.value() << "\n";
    if (Vendor.has_value()) oss << "Vendor: " << Vendor.value() << "\n";
    if (Profile.has_value()) oss << "Profile: " << Profile.value() << "\n";
    if (DriverVersion.has_value()) oss << "Driver Version: " << DriverVersion.value() << "\n";
    if (extensions.has_value()) oss << "Extensions: " << extensions.value() << "\n";
    if (PlatformId.has_value()) oss << "Platform Id: " << PlatformId.value() << "\n";
    if (MaxComputeUnits.has_value()) oss << "Max Compute Units: " << MaxComputeUnits.value() << "\n";
    if (MaxWorkItemDimensions.has_value()) oss << "Max Work Item Dimensions: " << MaxWorkItemDimensions.value() << "\n";
    if (MaxWorkItemSizes.has_value()) oss << "Max Work Item Sizes: " << vector_to_string(MaxWorkItemSizes.value()) << "\n";
    if (MaxWorkGroupSize.has_value()) oss << "Max Work Group Size: " << MaxWorkGroupSize.value() << "\n";
    if (PreferredVectorWidthChar.has_value()) oss << "Preferred Vector Width Char: " << PreferredVectorWidthChar.value() << " elements\n";
    if (PreferredVectorWidthShort.has_value()) oss << "Preferred Vector Width Short: " << PreferredVectorWidthShort.value() << " elements\n";
    if (PreferredVectorWidthInt.has_value()) oss << "Preferred Vector Width Int: " << PreferredVectorWidthInt.value() << " elements\n";
    if (PreferredVectorWidthLong.has_value()) oss << "Preferred Vector Width Long: " << PreferredVectorWidthLong.value() << " elements\n";
    if (PreferredVectorWidthFloat.has_value()) oss << "Preferred Vector Width Float: " << PreferredVectorWidthFloat.value() << " elements\n";
    if (PreferredVectorWidthDouble.has_value()) oss << "Preferred Vector Width Double: " << PreferredVectorWidthDouble.value() << " elements\n";
    if (PreferredVectorWidthHalf.has_value()) oss << "Preferred Vector Width Half: " << PreferredVectorWidthHalf.value() << " elements\n";
    if (NativeVectorWidthChar.has_value()) oss << "Native Vector Width Char: " << NativeVectorWidthChar.value() << " elements\n";
    if (NativeVectorWidthShort.has_value()) oss << "Native Vector Width Short: " << NativeVectorWidthShort.value() << " elements\n";
    if (NativeVectorWidthInt.has_value()) oss << "Native Vector Width Int: " << NativeVectorWidthInt.value() << " elements\n";
    if (NativeVectorWidthLong.has_value()) oss << "Native Vector Width Long: " << NativeVectorWidthLong.value() << " elements\n";
    if (NativeVectorWidthFloat.has_value()) oss << "Native Vector Width Float: " << NativeVectorWidthFloat.value() << " elements\n";
    if (NativeVectorWidthDouble.has_value()) oss << "Native Vector Width Double: " << NativeVectorWidthDouble.value() << " elements\n";
    if (NativeVectorWidthHalf.has_value()) oss << "Native Vector Width Half: " << NativeVectorWidthHalf.value() << " elements\n";
    if (MaxClockFrequency.has_value()) oss << "Max Clock Frequency: " << MaxClockFrequency.value() << "MHz\n";
    if (AddressBits.has_value()) oss << "Address Bits: " << AddressBits.value() << "\n";
    if (MaxMemAllocSize.has_value()) oss << "Max Memory Allocation Size: " << MaxMemAllocSize.value() << " bytes\n";
    if (MemBaseAddrAlign.has_value()) oss << "Memory Base Address Alignment: " << MemBaseAddrAlign.value() << "\n";
    if (MinDataTypeAlignSize.has_value()) oss << "Min Data Type Alignment Size: " << MinDataTypeAlignSize.value() << "\n";
    if (EndianLittle.has_value()) oss << "Endian Little: " << (EndianLittle.value() ? "True" : "False") << "\n";
    if (GlobalMemCacheType.has_value()) oss << "Global Memory Cache Type: " << global_memory_cache_type_to_string(GlobalMemCacheType.value()) << "\n";
    if (GlobalMemCachelineSize.has_value()) oss << "Global Memory Cacheline Size: " << GlobalMemCachelineSize.value() << " bytes\n";
    if (GlobalMemCacheSize.has_value()) oss << "Global Memory Cache Size: " << GlobalMemCacheSize.value() << " bytes\n";
    if (GlobalMemSize.has_value()) oss << "Global Memory Size: " << GlobalMemSize.value() << " bytes\n";
    if (MaxConstantBufferSize.has_value()) oss << "Max Constant Buffer Size: " << MaxConstantBufferSize.value() << " bytes\n";
    if (MaxConstantArgs.has_value()) oss << "Max Constant Arguments: " << MaxConstantArgs.value() << "\n";
    if (LocalMemType.has_value()) oss << "Local Memory Type: " << local_memory_type_to_string(LocalMemType.value()) << "\n";
    if (LocalMemSize.has_value()) oss << "Local Memory Size: " << LocalMemSize.value() << " bytes\n";
    if (ErrorCorrectionSupport.has_value()) oss << "Error Correction Support: " << (ErrorCorrectionSupport.value() ? "True" : "False") << "\n";
    if (HostUnifiedMemory.has_value()) oss << "Host Unified Memory: " << (HostUnifiedMemory.value() ? "True" : "False") << "\n";
    if (ImageSupport.has_value()) oss << "Image Support: " << (ImageSupport.value() ? "True" : "False") << "\n";
    if (MaxReadImageArgs.has_value()) oss << "Max Read Image Arguments: " << MaxReadImageArgs.value() << "\n";
    if (MaxWriteImageArgs.has_value()) oss << "Max Write Image Arguments: " << MaxWriteImageArgs.value() << "\n";
    if (Image2dMaxWidth.has_value()) oss << "Image 2D Max Width: " << Image2dMaxWidth.value() << " pixels\n";
    if (Image2dMaxHeight.has_value()) oss << "Image 2D Max Height: " << Image2dMaxHeight.value() << " pixels\n";
    if (Image3dMaxWidth.has_value()) oss << "Image 3D Max Width: " << Image3dMaxWidth.value() << " pixels\n";
    if (Image3dMaxHeight.has_value()) oss << "Image 3D Max Height: " << Image3dMaxHeight.value() << " pixels\n";
    if (Image3dMaxDepth.has_value()) oss << "Image 3D Max Depth: " << Image3dMaxDepth.value() << " pixels\n";
    if (MaxSamplers.has_value()) oss << "Max Samplers: " << MaxSamplers.value() << "\n";
    if (MaxParameterSize.has_value()) oss << "Max Parameter Size: " << MaxParameterSize.value() << " bytes\n";
    if (SingleFpConfig.has_value()) oss << "Single Floating Point Configuration:" << floating_point_config_to_string(SingleFpConfig.value()) << "\n";
    if (DoubleFpConfig.has_value()) oss << "Double Floating Point Configuration:" << floating_point_config_to_string(DoubleFpConfig.value()) << "\n";
    if (HalfFpConfig.has_value()) oss << "Half Floating Point Configuration:" << floating_point_config_to_string(HalfFpConfig.value()) << "\n";
    if (ProfilingTimerResolution.has_value()) oss << "Profiling Timer Resolution: " << ProfilingTimerResolution.value() << "\n";
    if (Available.has_value()) oss << "Available: " << Available.value() << "\n";
    if (CompilerAvailable.has_value()) oss << "Compiler Available: " << CompilerAvailable.value() << "\n";
    if (ExecutionCapabilities.has_value()) oss << "Execution Capabilities:\n" << execution_capabilities_to_string(ExecutionCapabilities.value()) << "\n";
    if (QueueProperties.has_value()) oss << "Queue Properties:\n" << queue_properties_to_string(QueueProperties.value());

    return oss.str();
  }

  std::string ClDeviceProperties::device_type_to_string(const cl_device_type type) const
  {
    switch(type)
    {
    case CL_DEVICE_TYPE_CPU: return "CPU";
    case CL_DEVICE_TYPE_GPU: return "GPU";
    case CL_DEVICE_TYPE_ACCELERATOR: return "Accelerator";
    default: return "Unknown";
    }
  }

  std::string ClDeviceProperties::global_memory_cache_type_to_string(const cl_device_mem_cache_type type) const
  {
    switch(type)
    {
    case CL_NONE: return "None";
    case CL_READ_ONLY_CACHE: return "Read Only";
    case CL_READ_WRITE_CACHE: return "Read/Write";
    default: return "Unkown";
    }
  }

  std::string ClDeviceProperties::local_memory_type_to_string(const cl_device_local_mem_type type) const
  {
    switch (type)
    {
    case CL_GLOBAL: return "Global";
    case CL_LOCAL: return "Local";
    default: return "Unknown";
    }
  }

  std::string ClDeviceProperties::execution_capabilities_to_string(const cl_device_exec_capabilities capabilities) const
  {
    std::ostringstream oss;

    oss << "Kernel: ";

    if (capabilities & CL_EXEC_KERNEL)
	    oss << "True";
    else
	    oss << "False";

    oss << "\nNative Kernel: ";

    if (capabilities & CL_EXEC_NATIVE_KERNEL)
	    oss << "True";
    else
	    oss << "False";

    return oss.str();
  }

  std::string ClDeviceProperties::queue_properties_to_string(const cl_command_queue_properties properties) const
  {
    std::ostringstream oss;

    oss << "Out of Order Execution: ";

    if (properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)
	    oss << "True";
    else
	    oss << "False";

    oss << "\nProfling: ";

    if (properties & CL_QUEUE_PROFILING_ENABLE)
	    oss << "True";
    else
	    oss << "False";

    return oss.str();
  }

  std::string ClDeviceProperties::floating_point_config_to_string(const cl_device_fp_config config) const
  {
    std::ostringstream oss;

    if (config & CL_FP_DENORM)
	    oss << "\nDenorms are supported";

    if (config & CL_FP_INF_NAN)
	    oss << "\nINF and NaNs are supported";

    if (config & CL_FP_ROUND_TO_NEAREST)
	    oss << "\nRound to nearest even rounding mode supported";

    if (config & CL_FP_ROUND_TO_ZERO)
	    oss << "\nRound to zero rounding mode is supported";

    if (config & CL_FP_ROUND_TO_INF)
	    oss << "\nRound to +ve and -ve infinity rounding modes is supported";

    if (config & CL_FP_FMA)
	    oss << "\nIEEE754-2008 fused multiply-add is supported";

    return oss.str();
  }

  std::string ClDeviceProperties::to_string(const cl_device_info property_name) const
  {
    switch (property_name)
    {
    case CL_DEVICE_ADDRESS_BITS: return "CL_DEVICE_ADDRESS_BITS";
    case CL_DEVICE_AVAILABLE: return "CL_DEVICE_AVAILABLE";
    case CL_DEVICE_COMPILER_AVAILABLE: return "CL_DEVICE_COMPILER_AVAILABLE";
    case CL_DEVICE_DOUBLE_FP_CONFIG: return "CL_DEVICE_DOUBLE_FP_CONFIG";
    case CL_DEVICE_ENDIAN_LITTLE: return "CL_DEVICE_ENDIAN_LITTLE";
    case CL_DEVICE_ERROR_CORRECTION_SUPPORT: return "CL_DEVICE_ERROR_CORRECTION_SUPPORT";
    case CL_DEVICE_EXECUTION_CAPABILITIES: return "CL_DEVICE_EXECUTION_CAPABILITIES";
    case CL_DEVICE_EXTENSIONS: return "CL_DEVICE_EXTENSIONS";
    case CL_DEVICE_GLOBAL_MEM_CACHE_SIZE: return "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE";
    case CL_DEVICE_GLOBAL_MEM_CACHE_TYPE: return "CL_DEVICE_GLOBAL_MEM_CACHE_TYPE";
    case CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE: return "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE";
    case CL_DEVICE_GLOBAL_MEM_SIZE: return "CL_DEVICE_GLOBAL_MEM_SIZE";
    case CL_DEVICE_HALF_FP_CONFIG: return "CL_DEVICE_HALF_FP_CONFIG";
    case CL_DEVICE_IMAGE_SUPPORT: return "CL_DEVICE_IMAGE_SUPPORT";
    case CL_DEVICE_IMAGE2D_MAX_HEIGHT: return "CL_DEVICE_IMAGE2D_MAX_HEIGHT";
    case CL_DEVICE_IMAGE2D_MAX_WIDTH: return "CL_DEVICE_IMAGE2D_MAX_WIDTH";
    case CL_DEVICE_IMAGE3D_MAX_DEPTH: return "CL_DEVICE_IMAGE3D_MAX_DEPTH";
    case CL_DEVICE_IMAGE3D_MAX_HEIGHT: return "CL_DEVICE_IMAGE3D_MAX_HEIGHT";
    case CL_DEVICE_IMAGE3D_MAX_WIDTH: return "CL_DEVICE_IMAGE3D_MAX_WIDTH";
    case CL_DEVICE_LOCAL_MEM_SIZE: return "CL_DEVICE_LOCAL_MEM_SIZE";
    case CL_DEVICE_LOCAL_MEM_TYPE: return "CL_DEVICE_LOCAL_MEM_TYPE";
    case CL_DEVICE_MAX_CLOCK_FREQUENCY: return "CL_DEVICE_MAX_CLOCK_FREQUENCY";
    case CL_DEVICE_MAX_COMPUTE_UNITS: return "CL_DEVICE_MAX_COMPUTE_UNITS";
    case CL_DEVICE_MAX_CONSTANT_ARGS: return "CL_DEVICE_MAX_CONSTANT_ARGS";
    case CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE: return "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE";
    case CL_DEVICE_MAX_MEM_ALLOC_SIZE: return "CL_DEVICE_MAX_MEM_ALLOC_SIZE";
    case CL_DEVICE_MAX_PARAMETER_SIZE: return "CL_DEVICE_MAX_PARAMETER_SIZE";
    case CL_DEVICE_MAX_READ_IMAGE_ARGS: return "CL_DEVICE_MAX_READ_IMAGE_ARGS";
    case CL_DEVICE_MAX_SAMPLERS: return "CL_DEVICE_MAX_SAMPLERS";
    case CL_DEVICE_MAX_WORK_GROUP_SIZE: return "CL_DEVICE_MAX_WORK_GROUP_SIZE";
    case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: return "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS";
    case CL_DEVICE_MAX_WORK_ITEM_SIZES: return "CL_DEVICE_MAX_WORK_ITEM_SIZES";
    case CL_DEVICE_MAX_WRITE_IMAGE_ARGS: return "CL_DEVICE_MAX_WRITE_IMAGE_ARGS";
    case CL_DEVICE_MEM_BASE_ADDR_ALIGN: return "CL_DEVICE_MEM_BASE_ADDR_ALIGN";
    case CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE: return "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE";
    case CL_DEVICE_NAME: return "CL_DEVICE_NAME";
    case CL_DEVICE_PLATFORM: return "CL_DEVICE_PLATFORM";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT";
    case CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE: return "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE";
    case CL_DEVICE_PROFILE: return "CL_DEVICE_PROFILE";
    case CL_DEVICE_PROFILING_TIMER_RESOLUTION: return "CL_DEVICE_PROFILING_TIMER_RESOLUTION";
    case CL_DEVICE_QUEUE_PROPERTIES: return "CL_DEVICE_QUEUE_PROPERTIES";
    case CL_DEVICE_SINGLE_FP_CONFIG: return "CL_DEVICE_SINGLE_FP_CONFIG";
    case CL_DEVICE_TYPE: return "CL_DEVICE_TYPE";
    case CL_DEVICE_VENDOR: return "CL_DEVICE_VENDOR";
    case CL_DEVICE_VENDOR_ID: return "CL_DEVICE_VENDOR_ID";
    case CL_DEVICE_VERSION: return "CL_DEVICE_VERSION";
    case CL_DRIVER_VERSION: return "CL_DRIVER_VERSION";
    default: return "CL_DEVICE_UNKNOWN_PROPERTY";
    }
  }

}  // malte
