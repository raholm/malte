#include <malte/opencl/buffer.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClBuffer::ClBuffer(const cl_mem_flags flags,
                     const std::size_t size,
                     const ClContext& context)
    : m_size(size)
  {
    Assert(!context.is_null());

    ClStatus status;
    m_handle = clCreateBuffer(context.get_handle(), flags, m_size, nullptr, status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClBuffer: " << status);
	    m_handle = nullptr;
    }
  }

  ClBuffer::ClBuffer(const cl_mem_flags flags,
                     const GLuint buffer,
                     const ClContext& context)
  {
    Assert(!context.is_null());
    Assert(buffer);

    ClStatus status;
    m_handle = clCreateFromGLBuffer(context.get_handle(), flags, buffer, status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClBuffer: " << status);
	    m_handle = nullptr;
    }
  }

  ClBuffer::ClBuffer(ClBuffer&& other)
  {
    std::swap(m_handle, other.m_handle);
    std::swap(m_size, other.m_size);
  }

  ClBuffer& ClBuffer::operator=(ClBuffer&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    std::swap(m_size, rhs.m_size);
    return *this;
  }

  ClBuffer::~ClBuffer()
  {
    destroy();
  }

  void ClBuffer::destroy()
  {
    if (!is_null())
    {
	    ClStatus status;
	    LogInternal("Destroying ClBuffer...");
	    status = clReleaseMemObject(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClBuffer: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClBuffer::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClBuffer::set_null()
  {
    m_handle = nullptr;
  }

  cl_mem ClBuffer::get_handle() const
  {
    return m_handle;
  }

  std::size_t ClBuffer::get_size() const
  {
    return m_size;
  }

}  // malte
