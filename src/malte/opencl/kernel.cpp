#include <malte/opencl/kernel.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClKernel::ClKernel(const ClProgram& program,
                     const std::string& name)
    : m_name(name)
  {
    Assert(!program.is_null());

    ClStatus status;
    m_handle = clCreateKernel(program.get_handle(), m_name.c_str(), status.get());

    if (status.is_error())
    {
	    LogError("Failed to create opencl kernel: " << status << ", name: " << name);
	    m_handle = nullptr;
    }
  }

  ClKernel::ClKernel(ClKernel&& other)
  {
    std::swap(m_handle, other.m_handle);
    std::swap(m_name, other.m_name);
  }

  ClKernel& ClKernel::operator=(ClKernel&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    std::swap(m_name, rhs.m_name);
    return *this;
  }

  ClKernel::~ClKernel()
  {
    destroy();
  }

  void ClKernel::destroy()
  {

    if (!is_null())
    {
	    LogInternal("Destroying ClKernel...");
	    ClStatus status;
	    status = clReleaseKernel(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClKernel: " << status);
	    else
        m_handle = nullptr;
    }
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const ClBuffer& buffer)
  {
    Assert(!buffer.is_null());
    cl_mem handle = buffer.get_handle();
    return set_arg(index, sizeof(handle), &handle);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const ClImage& image)
  {
    Assert(!image.is_null());
    cl_mem handle = image.get_handle();
    return set_arg(index, sizeof(handle), &handle);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const ClSampler& sampler)
  {
    Assert(!sampler.is_null());
    cl_sampler handle = sampler.get_handle();
    return set_arg(index, sizeof(handle), &handle);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const s32 value)
  {
    return set_arg(index, sizeof(value), &value);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const u32 value)
  {
    return set_arg(index, sizeof(value), &value);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const f32 value)
  {
    return set_arg(index, sizeof(value), &value);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const std::size_t value)
  {
    return set_arg(index, sizeof(value), &value);
  }

  ClStatus ClKernel::set_arg(const cl_uint index, const std::size_t size, const void *const data)
  {
    ClStatus status;
    status = clSetKernelArg(m_handle, index, size, data);

    if (status.is_error())
	    LogError("Failed to set argument " << index << " for kernel '" << m_name << "': " << status);

    return status;
  }

  bool ClKernel::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClKernel::set_null()
  {
    m_handle = nullptr;
  }

  cl_kernel ClKernel::get_handle() const
  {
    return m_handle;
  }

}  // malte
