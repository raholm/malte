#include <malte/opencl/program.h>

#include <algorithm>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClProgram::ClProgram(const std::vector<const char*>& kernel_sources,
                       const ClContext& context)
  {
    Assert(!context.is_null());

    ClStatus status;
    m_handle = clCreateProgramWithSource(context.get_handle(),
                                         kernel_sources.size(),
                                         (const char**) kernel_sources.data(),
                                         nullptr,
                                         status.get());

    if (status.is_error())
    {
	    LogError("Failed to create ClProgram: " << status);
	    m_handle = nullptr;
    }
  }

  ClProgram::ClProgram(ClProgram&& other)
  {
    std::swap(m_handle, other.m_handle);
  }

  ClProgram& ClProgram::operator=(ClProgram&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  ClProgram::~ClProgram()
  {
    destroy();
  }

  void ClProgram::destroy()
  {
    if (!is_null())
    {
	    LogInternal("Destroying ClProgram...");
	    ClStatus status;
	    status = clReleaseProgram(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClProgram: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClProgram::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClProgram::set_null()
  {
    m_handle = nullptr;
  }

  cl_program ClProgram::get_handle() const
  {
    return m_handle;
  }

  ClStatus ClProgram::build(const ClDevice& device,
                            const char *const compiler_flags,
                            const bool print_build_log_on_error) const
  {
    Assert(!device.is_null());

    ClStatus status;
    cl_device_id device_handle = device.get_handle();
    status = clBuildProgram(m_handle, 1, &device_handle, compiler_flags, nullptr, nullptr);

    if (status.is_error())
    {
	    LogError("Failed to build ClProgram: " << status);

	    if (print_build_log_on_error)
	    {
        std::size_t build_log_size;
        std::string build_log;

        ClStatus new_status;
        new_status = clGetProgramBuildInfo(m_handle,
                                           device_handle,
                                           CL_PROGRAM_BUILD_LOG,
                                           0,
                                           nullptr,
                                           &build_log_size);

        if (new_status.is_error())
        {
          LogError("Failed to get build log: " << new_status);
        } else
        {
          build_log.resize(build_log_size);
          new_status = clGetProgramBuildInfo(m_handle,
                                             device_handle,
                                             CL_PROGRAM_BUILD_LOG,
                                             build_log_size,
                                             (void*) build_log.c_str(),
                                             nullptr);

          if (new_status.is_error())
            LogError("Failed to get build log: " << new_status);
          else
            LogError("Build Log:\n" << build_log);
        }
	    }
    }

    return status;
  }

}  // malte
