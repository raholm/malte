#include <malte/opencl/context.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>
#include <malte/opengl/opengl_headers.h>

namespace malte {

  ClContext::ClContext(const ClPlatform& platform, const ClDevice& device, const bool opengl_sharing)
  {
    Assert(!platform.is_null());
    Assert(!device.is_null());

    std::vector<cl_context_properties> properties = {
	    CL_CONTEXT_PLATFORM, (cl_context_properties) platform.get_handle(),
    };

    if (opengl_sharing)
    {
	    properties.push_back(CL_GL_CONTEXT_KHR);
	    properties.push_back((cl_context_properties) glXGetCurrentContext());
	    properties.push_back(CL_GLX_DISPLAY_KHR);
	    properties.push_back((cl_context_properties) glXGetCurrentDisplay());
    }

    properties.push_back(0);

    const cl_device_id device_handle = device.get_handle();
    ClStatus status;
    m_handle = clCreateContext(properties.data(), 1, &device_handle, nullptr, nullptr, status.get());

    if (status.is_error())
    {
	    LogError("Failed to create opencl context: " << status);
	    m_handle = nullptr;
    }
  }

  ClContext::ClContext(const ClDevice& device, const bool opengl_sharing)
  {
    Assert(!device.is_null());

    std::vector<cl_context_properties> properties;

    if (opengl_sharing)
    {
	    properties.push_back(CL_GL_CONTEXT_KHR);
	    properties.push_back((cl_context_properties) glXGetCurrentContext());
	    properties.push_back(CL_GLX_DISPLAY_KHR);
	    properties.push_back((cl_context_properties) glXGetCurrentDisplay());
    }

    properties.push_back(0);

    const cl_device_id device_handle = device.get_handle();
    ClStatus status;
    m_handle = clCreateContext(properties.data(), 1, &device_handle, nullptr, nullptr, status.get());

    if (status.is_error())
    {
	    LogError("Failed to create opencl context: " << status);
	    m_handle = nullptr;
    }
  }

  ClContext::ClContext(ClContext&& other)
  {
    std::swap(m_handle, other.m_handle);
  }

  ClContext& ClContext::operator=(ClContext&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    return *this;
  }

  ClContext::~ClContext()
  {
    destroy();
  }

  void ClContext::destroy()
  {
    if (!is_null())
    {
	    LogInternal("Destroying opencl context...");
	    ClStatus status;
	    status = clReleaseContext(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy opencl context: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClContext::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClContext::set_null()
  {
    m_handle = nullptr;
  }

  cl_context ClContext::get_handle() const
  {
    return m_handle;
  }

}  // malte
