#include <malte/opencl/pipe.h>

#include <malte/core/debug/assert.h>
#include <malte/core/debug/log.h>

namespace malte {

  ClPipe::ClPipe(const cl_mem_flags flags,
                 const cl_uint packet_size,
                 const cl_uint max_packets,
                 const ClContext& context)
    : m_flags(flags),
      m_packet_size(packet_size),
      m_max_packets(max_packets)
  {
    Assert(!context.is_null());

    ClStatus status;
    m_handle = clCreatePipe(context.get_handle(),
                            m_flags,
                            m_packet_size,
                            m_max_packets,
                            nullptr,
                            status.get());

    if (status.is_error())
    {
	    LogError("Failed to create opencl ClPipe: " << status);
	    m_handle = nullptr;
    }
  }

  ClPipe::ClPipe(ClPipe&& other)
  {
    std::swap(m_handle, other.m_handle);
    std::swap(m_flags, other.m_flags);
    std::swap(m_packet_size, other.m_packet_size);
    std::swap(m_max_packets, other.m_max_packets);
  }

  ClPipe& ClPipe::operator=(ClPipe&& rhs)
  {
    std::swap(m_handle, rhs.m_handle);
    std::swap(m_flags, rhs.m_flags);
    std::swap(m_packet_size, rhs.m_packet_size);
    std::swap(m_max_packets, rhs.m_max_packets);
    return *this;
  }

  ClPipe::~ClPipe()
  {
    destroy();
  }

  void ClPipe::destroy()
  {
    if (!is_null())
    {
	    LogInternal("Destroying opencl ClPipe...");
	    ClStatus status;
	    status = clReleaseMemObject(m_handle);

	    if (status.is_error())
        LogError("Failed to destroy ClPipe: " << status);
	    else
        m_handle = nullptr;
    }
  }

  bool ClPipe::is_null() const
  {
    return m_handle == nullptr;
  }

  void ClPipe::set_null()
  {
    m_handle = nullptr;
  }

  cl_mem ClPipe::get_handle() const
  {
    return m_handle;
  }

  cl_mem_flags ClPipe::get_flags() const
  {
    return m_flags;
  }

  cl_uint ClPipe::get_packet_size() const
  {
    return m_packet_size;
  }

  cl_uint ClPipe::get_max_packets() const
  {
    return m_max_packets;
  }

}  // malte
