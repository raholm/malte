#include <malte/opencl/kernel_source_manager.h>

#include <malte/core/io.h>

namespace malte {

  ClKernelSourceManager::ClKernelSourceManager(ClKernelSourceManager&& other)
  {
    std::swap(m_source_infos, other.m_source_infos);
  }

  ClKernelSourceManager& ClKernelSourceManager::operator=(ClKernelSourceManager&& rhs)
  {
    std::swap(m_source_infos, rhs.m_source_infos);
    return *this;
  }

  void ClKernelSourceManager::destroy()
  {
    m_source_infos.clear();
  }

  bool ClKernelSourceManager::add_kernel(const std::string& program_name,
                                         const std::string& kernel_name)
  {
    if (is_invalid_program_name(program_name))
    {
	    LogWarning("Adding source file to invalid program name: " << program_name);
	    return false;
    }

    const std::string source_file = get_file_path(kernel_name);

    if (has_source_file(program_name, source_file))
    {
	    LogWarning("Adding duplicate source file to '" << program_name << "': " << source_file);
	    return false;
    }

    if (!file_exists(source_file))
    {
	    LogWarning("Adding source file that does not exist to '" << program_name << "': " << source_file);
	    return false;
    }

    SourceInfo source_info;
    source_info.file = source_file;

    m_source_infos[program_name].push_back(source_info);
    return true;
  }

  std::vector<const char*> ClKernelSourceManager::get_source_code(const std::string& program_name)
  {
    auto It = m_source_infos.find(program_name);

    if (It == m_source_infos.end())
    {
	    LogWarning("Trying to get sources for unknown program: " << program_name);
	    return std::vector<const char*>();
    }

    std::vector<SourceInfo>& source_infos = (It->second);
    std::vector<const char*> kernel_sources;
    kernel_sources.reserve(source_infos.size());

    for (SourceInfo& source_info : source_infos)
    {
	    if (source_info.code == "")
	    {
        std::optional<std::string> source_code = read_source_code_from_file(source_info.file);
        if (!source_code.has_value())
          continue;

        source_info.code = source_code.value();
	    }

	    kernel_sources.push_back(source_info.code.c_str());
    }

    return kernel_sources;
  }

  bool ClKernelSourceManager::has_source_code(const std::string& program_name) const
  {
    auto It = m_source_infos.find(program_name);
    return It != m_source_infos.end();
  }

  std::string ClKernelSourceManager::get_file_path(const std::string& kernel_name) const
  {
    return "./resources/kernels/" + kernel_name + ".cl";
  }

  bool ClKernelSourceManager::has_source_file(const std::string& program_name, const std::string& source_file) const
  {
    auto It = m_source_infos.find(program_name);

    if (It == m_source_infos.end())
	    return false;

    for (const SourceInfo& source_info : It->second)
	    if (source_info.file == source_file)
        return true;

    return false;
  }

  bool ClKernelSourceManager::is_invalid_program_name(const std::string& program_name) const
  {
    return program_name.empty();
  }

  std::optional<std::string> ClKernelSourceManager::read_source_code_from_file(const std::string& source_file) const
  {
    return read_text_file_content(source_file);
  }

}  // malte
