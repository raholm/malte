#pragma once

#include <vector>
#include <string>
#include <optional>

#include "opencl_headers.h"
#include "device.h"

#include <malte/core/interface/nullable.h>

namespace malte {

  class ClPlatform;

  class ClPlatformProperties final
  {
  public:
    ClPlatformProperties(const ClPlatform& Platform);

    ~ClPlatformProperties() = default;

    std::string to_string() const;

  public:
    std::optional<std::string> name;
    std::optional<std::string> profile;
    std::optional<std::string> vendor;
    std::optional<std::string> version;
    std::optional<std::string> extensions;

  private:
    std::optional<std::string> get_property_string(const cl_platform_id platform_id,
                                                   const cl_platform_info property_id) const;
    std::string property_id_to_string(const cl_platform_info property_id) const;

  };

  class ClPlatform final : public INullable
  {
  public:
    ClPlatform() = default;
    ClPlatform(const cl_platform_id handle);

    ~ClPlatform() = default;

    std::vector<ClDevice> get_devices(const cl_device_type type = CL_DEVICE_TYPE_ALL) const;
    ClDevice get_opengl_device() const;

    bool is_null() const override;
    void set_null() override;

    cl_platform_id get_handle() const;
    ClPlatformProperties get_properties() const;

  private:
    cl_platform_id m_handle { nullptr };

    std::string get_device_type_string(const cl_device_type type) const;

  };

  std::vector<ClPlatform> get_cl_platforms(const bool opengl_sharing = false);
  ClPlatform get_cl_platform(const std::string& vendor, const bool opengl_sharing = false);

}  // malte
