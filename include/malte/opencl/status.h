#pragma once

#include <string>
#include <iostream>

#include "opencl_headers.h"

namespace malte {

  class ClStatus final
  {
  public:
    ClStatus();
    ClStatus(const cl_int status_code);

    ~ClStatus() = default;

    ClStatus& operator=(const cl_int status_code);

    cl_int* get();

    bool is_ok() const;
    bool is_error() const;

    const char* to_string() const;

  private:
    cl_int m_status;

  };

  inline std::ostream& operator<<(std::ostream& out, const ClStatus status)
  {
    out << status.to_string();
    return out;
  }

}  // malte
