#pragma once

#include <string>
#include <optional>
#include <vector>
#include <sstream>

#include "opencl_headers.h"
#include "status.h"

#include <malte/core/debug/log.h>
#include <malte/core/interface/nullable.h>
#include <malte/core/types.h>

namespace malte {

  class ClDevice;

  class ClDeviceProperties final
  {
  public:
    ClDeviceProperties(const ClDevice& Device);

    ~ClDeviceProperties() = default;

    std::string to_string() const;

  public:
    std::optional<std::string> Name;
    std::optional<u64> Type;
    std::optional<std::string> Version;
    std::optional<u32> VendorId;
    std::optional<std::string> Vendor;
    std::optional<std::string> Profile;
    std::optional<std::string> DriverVersion;
    std::optional<std::string> extensions;
    std::optional<cl_platform_id> PlatformId;

    std::optional<u32> MaxComputeUnits;
    std::optional<u32> MaxWorkItemDimensions;
    std::optional<std::vector<std::size_t>> MaxWorkItemSizes;
    std::optional<std::size_t> MaxWorkGroupSize;

    std::optional<u32> PreferredVectorWidthChar;
    std::optional<u32> PreferredVectorWidthShort;
    std::optional<u32> PreferredVectorWidthInt;
    std::optional<u32> PreferredVectorWidthLong;
    std::optional<u32> PreferredVectorWidthFloat;
    std::optional<u32> PreferredVectorWidthDouble;
    std::optional<u32> PreferredVectorWidthHalf;

    std::optional<u32> NativeVectorWidthChar;
    std::optional<u32> NativeVectorWidthShort;
    std::optional<u32> NativeVectorWidthInt;
    std::optional<u32> NativeVectorWidthLong;
    std::optional<u32> NativeVectorWidthFloat;
    std::optional<u32> NativeVectorWidthDouble;
    std::optional<u32> NativeVectorWidthHalf;

    std::optional<u32> MaxClockFrequency;
    std::optional<u32> AddressBits;

    std::optional<u64> MaxMemAllocSize;
    std::optional<u32> MemBaseAddrAlign;
    std::optional<u32> MinDataTypeAlignSize;
    std::optional<u32> EndianLittle;

    std::optional<u32> GlobalMemCacheType;
    std::optional<u32> GlobalMemCachelineSize;
    std::optional<u64> GlobalMemCacheSize;
    std::optional<u64> GlobalMemSize;
    std::optional<u64> MaxConstantBufferSize;
    std::optional<u32> MaxConstantArgs;
    std::optional<u32> LocalMemType;
    std::optional<u64> LocalMemSize;
    std::optional<u32> ErrorCorrectionSupport;
    std::optional<u32> HostUnifiedMemory;

    std::optional<u32> ImageSupport;
    std::optional<u32> MaxReadImageArgs;
    std::optional<u32> MaxWriteImageArgs;
    std::optional<std::size_t> Image2dMaxWidth;
    std::optional<std::size_t> Image2dMaxHeight;
    std::optional<std::size_t> Image3dMaxWidth;
    std::optional<std::size_t> Image3dMaxHeight;
    std::optional<std::size_t> Image3dMaxDepth;

    std::optional<u32> MaxSamplers;
    std::optional<std::size_t> MaxParameterSize;
    std::optional<u64> SingleFpConfig;
    std::optional<u64> DoubleFpConfig;
    std::optional<u64> HalfFpConfig;

    std::optional<std::size_t> ProfilingTimerResolution;
    std::optional<u32> Available;
    std::optional<u32> CompilerAvailable;
    std::optional<u64> ExecutionCapabilities;
    std::optional<u64> QueueProperties;

  private:
    template<typename T>
    std::optional<T> get_property_value(const cl_device_id device_id,
                                        const cl_device_info property_name) const;

    template<typename T>
    std::optional<std::vector<T>> get_property_value_vector(const cl_device_id device_id,
                                                            const cl_device_info property_name) const;

    template<typename T>
    std::string vector_to_string(const std::vector<T>& Vector) const;

    std::string device_type_to_string(const cl_device_type type) const;
    std::string global_memory_cache_type_to_string(const cl_device_mem_cache_type type) const;
    std::string floating_point_config_to_string(const cl_device_fp_config config) const;
    std::string local_memory_type_to_string(const cl_device_local_mem_type type) const;
    std::string execution_capabilities_to_string(const cl_device_exec_capabilities capabilities) const;
    std::string queue_properties_to_string(const cl_command_queue_properties properties) const;

    std::string to_string(const cl_device_info property_name) const;

  };

  class ClDevice final : public INullable
  {
  public:
    enum Type
    {
	    GPU = CL_DEVICE_TYPE_GPU,
	    CPU = CL_DEVICE_TYPE_CPU,
    };

  public:
    ClDevice() = default;
    ClDevice(const cl_device_id handle);

    ~ClDevice() = default;

    bool is_null() const override;
    void set_null() override;

    cl_device_id get_handle() const;
    ClDeviceProperties get_properties() const;

  private:
    cl_device_id m_handle { nullptr };

  };

}  // malte

#include "device.inl"
