#pragma once

#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClKernelSourceManager : public IDestroyable,
                                private INonCopyable
  {
  public:
    ClKernelSourceManager() = default;

    ClKernelSourceManager(ClKernelSourceManager&& other);
    ClKernelSourceManager& operator=(ClKernelSourceManager&& rhs);

    ~ClKernelSourceManager() = default;

    void destroy() override;

    bool add_kernel(const std::string& program_name,
                    const std::string& kernel_name);

    std::vector<const char*> get_source_code(const std::string& program_name);

    bool has_source_code(const std::string& program_name) const;

  private:
    struct SourceInfo
    {
	    std::string file;
	    std::string code;
    };

    std::unordered_map<std::string, std::vector<SourceInfo>> m_source_infos;

    std::string get_file_path(const std::string& kernel_name) const;
    bool has_source_file(const std::string& program_name, const std::string& source_file) const;
    bool is_invalid_program_name(const std::string& program_name) const;

    std::optional<std::string> read_source_code_from_file(const std::string& source_file) const;

  };

}  // malte
