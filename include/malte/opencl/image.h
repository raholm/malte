#pragma once

#include "opencl_headers.h"
#include "context.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

	class ClImage final : public INullable, IDestroyable,
                        private INonCopyable
	{
	public:
    ClImage(const cl_mem_flags flags,
            const cl_image_desc description,
            const cl_image_format format,
            const ClContext& context);

    ClImage(ClImage&& other);
    ClImage& operator=(ClImage&& rhs);

    ~ClImage();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    cl_mem get_handle() const;
    cl_image_desc get_description() const;
    cl_image_format get_format() const;
    cl_mem_flags get_flags() const;

	public:
    cl_mem m_handle;
    cl_image_desc m_description;
    cl_image_format m_format;
    cl_mem_flags m_flags;

	};

}  // malte
