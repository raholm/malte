#pragma once

#include "opencl_headers.h"
#include "context.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClPipe final : public INullable, IDestroyable,
                       private INonCopyable
  {
  public:
    ClPipe(const cl_mem_flags flags,
           const cl_uint packet_size,
           const cl_uint max_packets,
           const ClContext& context);

    ClPipe(ClPipe&& other);
    ClPipe& operator=(ClPipe&& rhs);

    ~ClPipe();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    cl_mem get_handle() const;
    cl_mem_flags get_flags() const;
    cl_uint get_packet_size() const;
    cl_uint get_max_packets() const;

  private:
    cl_mem m_handle { nullptr };
    cl_mem_flags m_flags;
    cl_uint m_packet_size;
    cl_uint m_max_packets;

  };

}  // malte
