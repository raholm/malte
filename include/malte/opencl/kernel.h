#pragma once

#include <string.h>
#include <vector>
#include <array>

#include "opencl_headers.h"
#include "buffer.h"
#include "image.h"
#include "program.h"
#include "sampler.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>
#include <malte/core/types.h>

namespace malte {

  class ClKernel final : public INullable, IDestroyable,
                         private INonCopyable
  {
  public:
    ClKernel(const ClProgram& program,
             const std::string& name);

    ClKernel(ClKernel&& other);
    ClKernel& operator=(ClKernel&& rhs);

    ~ClKernel();

    void destroy() override;

    ClStatus set_arg(const cl_uint index, const ClBuffer& buffer);
    ClStatus set_arg(const cl_uint index, const ClImage& image);
    ClStatus set_arg(const cl_uint index, const ClSampler& sampler);
    ClStatus set_arg(const cl_uint index, const s32 value);
    ClStatus set_arg(const cl_uint index, const u32 value);
    ClStatus set_arg(const cl_uint index, const f32 value);
    ClStatus set_arg(const cl_uint index, const std::size_t value);
    ClStatus set_arg(const cl_uint index, const std::size_t size, const void *const data);

    template<typename T>
    ClStatus set_arg(const cl_uint index, const std::vector<T>& values);

    template<typename T, const std::size_t Size>
    ClStatus set_arg(const cl_uint index, const std::array<T, Size>& values);

    bool is_null() const override;
    void set_null() override;

    cl_kernel get_handle() const;
    std::string get_name() const;

  private:
    cl_kernel m_handle;
    std::string m_name;

  };

  template<typename T>
  inline ClStatus ClKernel::set_arg(const cl_uint index, const std::vector<T>& values)
  {
    return set_arg(index, values.size() * sizeof(T), values.data());
  }

  template<typename T, const std::size_t Size>
  inline ClStatus ClKernel::set_arg(const cl_uint index, const std::array<T, Size>& values)
  {
    return set_arg(index, values.size() * sizeof(T), values.data());
  }

}  // malte
