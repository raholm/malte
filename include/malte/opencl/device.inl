namespace malte {

  template<typename T>
  std::optional<T> ClDeviceProperties::get_property_value(const cl_device_id device_id,
                                                          const cl_device_info property_name) const
  {
    ClStatus status;
    std::size_t size;
    T output;

    status = clGetDeviceInfo(device_id,
                             property_name,
                             sizeof(T),
                             &output,
                             &size);

    if (status.is_error())
    {
	    LogError("Failed to get property value '" << to_string(property_name) << "': " << status);
	    return {};
    }

    return output;
  }

  template<typename T>
  std::optional<std::vector<T>> ClDeviceProperties::get_property_value_vector(const cl_device_id device_id,
                                                                              const cl_device_info property_name) const
  {
    ClStatus status;
    std::size_t size;
    std::vector<T> output;

    status = clGetDeviceInfo(device_id,
                             property_name,
                             0,
                             nullptr,
                             &size);

    if (status.is_error())
    {
	    LogError("Failed to get property size for '" << to_string(property_name) << "': " << status);
	    return {};
    }

    output.resize(size / sizeof(T));
    status = clGetDeviceInfo(device_id,
                             property_name,
                             size,
                             output.data(),
                             nullptr);

    if (status.is_error())
    {
	    LogError("Failed to get property '" << to_string(property_name) << "': " << status);
	    return {};
    }

    return output;
  }

  template<typename T>
  std::string ClDeviceProperties::vector_to_string(const std::vector<T>& vector) const
  {
    std::ostringstream oss;
    auto last_index = vector.size() - 1;
    oss << "(";

    for (std::size_t index = 0; index < vector.size(); ++index)
    {
	    oss << vector[index];
	    if (index < last_index)
        oss << ", ";
    }

    oss << ")";
    return oss.str();
  }

}  // malte
