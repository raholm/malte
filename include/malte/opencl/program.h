#pragma once

#include <string>
#include <vector>

#include "opencl_headers.h"
#include "context.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClProgram final : public INullable, IDestroyable,
                          private INonCopyable
  {
  public:
    ClProgram() = default;
    ClProgram(const std::vector<const char*>& kernel_sources,
              const ClContext& context);

    ClProgram(ClProgram&& other);
    ClProgram& operator=(ClProgram&& rhs);

    ~ClProgram();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    cl_program get_handle() const;

    ClStatus build(const ClDevice& device,
                   const char *const compile_flags = nullptr,
                   const bool print_build_log_on_error = true) const;

  private:
    cl_program m_handle { nullptr };

  };

}  // malte
