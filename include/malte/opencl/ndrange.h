#pragma once

#include <cstddef>

namespace malte {

  struct ClNDRange
  {
    constexpr explicit ClNDRange(const std::size_t _x,
                                 const std::size_t _y = 1,
                                 const std::size_t _z = 1)
	    : x(_x),
	      y(_y),
	      z(_z)
	    {}

    ~ClNDRange() = default;

    inline std::size_t get_dim() const { return 3; }
    inline const std::size_t* get_data() const { return (std::size_t*) this; }

    std::size_t x;
    std::size_t y;
    std::size_t z;
  };

  constexpr ClNDRange NullClNDRange(0, 0, 0);

}  // malte
