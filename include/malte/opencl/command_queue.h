#pragma once

#include "opencl_headers.h"
#include "context.h"
#include "device.h"
#include "status.h"
#include "buffer.h"
#include "ndrange.h"
#include "kernel.h"
#include "image.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClCommandQueue final : public INullable, IDestroyable,
                               private INonCopyable
  {
  public:
    ClCommandQueue() = default;
    ClCommandQueue(const ClDevice& device,
                   const ClContext& context);

    ClCommandQueue(ClCommandQueue&& other);
    ClCommandQueue& operator=(ClCommandQueue&& rhs);

    ~ClCommandQueue();

    void destroy() override;

    // TODO(ra-hol) : Add enqueue functions
    // Can I template these? (Size/Data)
    ClStatus write_buffer(const ClBuffer& buffer,
                          const bool synchronize,
                          const std::size_t offset,
                          const std::size_t size,
                          const void *const data);

    ClStatus read_buffer(const ClBuffer& buffer,
                         const bool synchronize,
                         const std::size_t offset,
                         const std::size_t size,
                         void *const data);

    ClStatus fill_buffer(const ClBuffer& buffer,
                         const std::size_t pattern_size,
                         const void *const pattern,
                         const std::size_t offset,
                         const std::size_t size);

    ClStatus run_kernel(const ClKernel& kernel,
                        const ClNDRange global_work_size,
                        const ClNDRange local_work_size,
                        const ClNDRange global_work_offset = NullClNDRange);

    ClStatus write_image(const ClImage& image,
                         const bool synchronize,
                         const ClNDRange origin,
                         const ClNDRange region,
                         void *const data,
                         const std::size_t row_pitch = 0,
                         const std::size_t slice_pitch = 0);

    ClStatus read_image(const ClImage& image,
                        const bool synchronize,
                        const ClNDRange origin,
                        const ClNDRange region,
                        void *const data,
                        const std::size_t row_pitch = 0,
                        const std::size_t slice_pitch = 0);

    ClStatus acquire_buffer(const ClBuffer& buffer);
    ClStatus release_buffer(const ClBuffer& buffer);

    ClStatus flush();
    ClStatus finish();
    ClStatus flush_and_finish();

    bool is_null() const override;
    void set_null() override;

    cl_command_queue get_handle() const;

  private:
    cl_command_queue m_handle { nullptr };

  };

}  // malte
