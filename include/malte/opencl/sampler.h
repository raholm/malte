#pragma once

#include "opencl_headers.h"
#include "context.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  struct ClSamplerProperties
  {
    cl_sampler_properties normalized_coordinates { CL_TRUE };
    cl_sampler_properties addressing_mode { CL_ADDRESS_CLAMP };
    cl_sampler_properties filter_mode { CL_FILTER_NEAREST };
  };

  class ClSampler final : public INullable, IDestroyable,
                          private INonCopyable
  {
  public:
    ClSampler(const ClSamplerProperties& properties,
              const ClContext& context);

    ClSampler(ClSampler&& other);
    ClSampler& operator=(ClSampler&& rhs);

    ~ClSampler();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    const ClSamplerProperties& get_properties() const;
    cl_sampler get_handle() const;

  private:
    ClSamplerProperties m_properties;
    cl_sampler m_handle;

  };

}  // malte
