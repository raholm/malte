#pragma once

#include "opencl_headers.h"
#include "device.h"
#include "platform.h"
#include "status.h"

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClContext final : public INullable, IDestroyable,
                          private INonCopyable
  {
  public:
    ClContext() = default;
    ClContext(const ClPlatform& platform,
              const ClDevice& device,
              const bool opengl_sharing = false);
    ClContext(const ClDevice& device,
              const bool opengl_sharing = false);

    ClContext(ClContext&& other);
    ClContext& operator=(ClContext&& rhs);

    ~ClContext();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    cl_context get_handle() const;

  private:
    cl_context m_handle { nullptr };

  };

}  // malte
