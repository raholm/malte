#pragma once

#include "opencl_headers.h"
#include "context.h"

#include <malte/opengl/opengl_headers.h>

#include <malte/core/interface/nullable.h>
#include <malte/core/interface/destroyable.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class ClBuffer final : public INullable, IDestroyable,
                         private INonCopyable
  {
  public:
    enum Mode
    {
	    Read = CL_MEM_READ_ONLY,
	    ReadWrite = CL_MEM_READ_WRITE,
	    Write = CL_MEM_WRITE_ONLY,
    };

  public:
    ClBuffer(const cl_mem_flags flags,
             const std::size_t size,
             const ClContext& context);
    // @todo we probably want to make a ClGraphicsBuffer
    // instead to make it more clear what kind of buffer it is.
    // This also makes it more clear the we have to acquire/release
    // in the command queue
    ClBuffer(const cl_mem_flags flags,
             const GLuint buffer,
             const ClContext& context);

    ClBuffer(ClBuffer&& other);
    ClBuffer& operator=(ClBuffer&& rhs);

    ~ClBuffer();

    void destroy() override;

    bool is_null() const override;
    void set_null() override;

    cl_mem get_handle() const;
    std::size_t get_size() const;

  public:
    cl_mem m_handle { nullptr };
    std::size_t m_size { 0 };

  };

}  // malte
