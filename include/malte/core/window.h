#pragma once

#include <string>
#include <vector>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <malte/core/types.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class Window : private INonCopyable
  {
  public:
    Window(const u32 width,
           const u32 height,
           const std::string& title);

    Window(Window&& other);
    Window& operator=(Window&& rhs);

    ~Window();

    void poll_events() const;
    void swap_buffers() const;

    void set_title(const std::string& title);

    u32 get_width() const;
    u32 get_height() const;
    u32 get_screen_width() const;
    u32 get_screen_height() const;
    std::string get_title() const;
    GLFWwindow* get_handle() const;

    bool is_null() const;
    bool is_open() const;

  private:
    u32 m_width;
    u32 m_height;
    s32 m_screen_width;
    s32 m_screen_height;
    std::string m_title;
    GLFWwindow* m_handle { nullptr };

  };

}  // malte
