#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <malte/core/debug/log.h>

namespace malte {

  inline bool file_exists(const std::string& file_path)
  {
    std::ifstream file(file_path, std::ifstream::in);
    return file.good();
  }

  inline std::optional<std::string> read_text_file_content(const std::string& file_path)
  {
    std::ifstream file(file_path);

    if (!file.is_open() || !file.good())
    {
	    LogError("Failed to open file " << file_path);
	    return {};
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    return { buffer.str() };
  }

}  // malte
