#pragma once

#include <iostream>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <thread>
#include <mutex>

#include <malte/core/types.h>

namespace malte {

  enum class LogLevel
  {
    Internal,
    Debug,
    Info,
    Warning,
    Error,
    Critical
  };

  inline std::string current_date_time_string()
  {
    auto now = std::chrono::system_clock::now();
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
    auto timer = std::chrono::system_clock::to_time_t(now);

    std::ostringstream output;
    output << std::put_time(std::localtime(&timer), "%Y-%m-%d %H:%M:%S") << "."
           << std::setfill('0') << std::setw(3) << milliseconds.count();
    return output.str();
  }

  inline std::string to_string(const LogLevel log_level)
  {
    switch(log_level)
    {
    case LogLevel::Internal:
	    return "INTERNAL";
    case LogLevel::Debug:
	    return "DEBUG";
    case LogLevel::Info:
	    return "INFO";
    case LogLevel::Warning:
	    return "WARNING";
    case LogLevel::Error:
	    return "ERROR";
    case LogLevel::Critical:
	    return "CRITICAL";
    default:
	    return "UNKOWN";
    }
  }

  inline void log(const LogLevel log_level,
                  const std::string& message,
                  const std::string& file_path,
                  const u32 line_number)
  {
    static std::mutex mutex;
    std::lock_guard<std::mutex> lock_guard(mutex);
    auto file_name_start_index = file_path.find_last_of("/\\") + 1;
    auto file_name = file_path.substr(file_name_start_index);

    std::cerr << "[" << std::left << std::setw(22) << current_date_time_string() << "|"
              << std::right << std::setw(8) << to_string(log_level) << "|"
              << "T" << std::this_thread::get_id() << "|"
              << file_name << ":" << line_number << "]$ "
              << message << std::endl;
  }

} // malte

#define FormatMessage(Items) ((dynamic_cast<std::ostringstream&>(std::ostringstream().seekp(0, std::ios_base::cur) << Items)).str())
#define LogInternal(Message) malte::log(malte::LogLevel::Internal, FormatMessage(Message), __FILE__, __LINE__)
#define LogDebug(Message) malte::log(malte::LogLevel::Debug, FormatMessage(Message), __FILE__, __LINE__)
#define LogInfo(Message) malte::log(malte::LogLevel::Info, FormatMessage(Message), __FILE__, __LINE__)
#define LogWarning(Message) malte::log(malte::LogLevel::Warning, FormatMessage(Message), __FILE__, __LINE__)
#define LogError(Message) malte::log(malte::LogLevel::Error, FormatMessage(Message), __FILE__, __LINE__)
#define LogCritical(Message) malte::log(malte::LogLevel::Critical, FormatMessage(Message), __FILE__, __LINE__)
