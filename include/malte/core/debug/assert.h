#pragma once

#include <stdio.h>
#include <cassert>

#define Assert(Expression) assert((Expression))
