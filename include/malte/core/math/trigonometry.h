#pragma once

#include "../types.h"

namespace malte {

  constexpr const f32 k_pi = 3.141592f;
  constexpr const f32 k_radians_per_degree = 0.017453292519943295769237f;
  constexpr const f32 k_degrees_per_radian = 57.295779513082320876798155f;

  constexpr inline f32 radians(const f32 degrees)
  {
    return degrees * static_cast<f32>(k_radians_per_degree);
  }

  constexpr inline f32 degrees(const f32 radians)
  {
    return radians * static_cast<f32>(k_degrees_per_radian);
  }

}  // malte
