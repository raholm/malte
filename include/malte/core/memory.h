#pragma once

#include <vector>
#include <array>

#include <malte/core/types.h>

namespace malte {

  template<typename T>
  constexpr inline void* DataPtr(const T& data)
  {
    return (void*) &data;
  }

  template<typename T>
  constexpr inline u32 DataSize(const T& data)
  {
    return sizeof(T);
  }

  template<typename T>
  constexpr inline void* DataPtr(const T *const data)
  {
    return (void*) data;
  }

  template<typename T>
  constexpr inline u32 DataSize(const T *const data)
  {
    return sizeof(data) * sizeof(T);
  }

  template<typename T>
  constexpr inline void* DataPtr(const std::vector<T>& data)
  {
    return (void*) data.data();
  }

  template<typename T>
  constexpr inline u32 DataSize(const std::vector<T>& data)
  {
    return data.size() * sizeof(T);
  }

  template<typename T, const std::size_t Size>
  constexpr inline void* DataPtr(const std::array<T, Size>& data)
  {
    return (void*) data.data();
  }

  template<typename T, const std::size_t Size>
  constexpr inline u32 DataSize(const std::array<T, Size>& data)
  {
    return data.size() * sizeof(T);
  }

}  // malte
