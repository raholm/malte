#pragma once

namespace malte {

  class IDestroyable
  {
  public:
    virtual ~IDestroyable() = default;
    virtual void destroy() = 0;
  };

}  // malte
