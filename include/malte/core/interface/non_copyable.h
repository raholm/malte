#pragma once

namespace malte {

  class INonCopyable
  {
  protected:
    INonCopyable() = default;
    virtual ~INonCopyable() = default;

  private:
    INonCopyable(const INonCopyable& Other) = delete;
    INonCopyable& operator=(const INonCopyable& Rhs) = delete;
  };

}  // malte
