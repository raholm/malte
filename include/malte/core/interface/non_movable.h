#pragma once

namespace malte {

  class INonMovable
  {
  protected:
    INonMovable() = default;
    virtual ~INonMovable() = default;

  private:
    INonMovable(INonMovable&& Other) = delete;
    INonMovable& operator=(INonMovable&& Rhs) = delete;
  };

}  // malte
