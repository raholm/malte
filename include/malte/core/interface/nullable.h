#pragma once

namespace malte {

  class INullable
  {
  public:
    virtual ~INullable() = default;
    virtual bool is_null() const = 0;
    virtual void set_null() = 0;
  };

}  // malte
