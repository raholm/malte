#pragma once

#include <vector>
#include <random>

#include <malte/core/types.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class UniformSampler
  {
  public:
    using SeedType = std::mt19937::result_type;

  public:
    UniformSampler();
    UniformSampler(const SeedType Seed);

    ~UniformSampler() = default;

    template<typename T>
    inline T next(const T min, const T max);

  private:
    std::mt19937 m_random_generator;

  };

}  // malte

#include "details/uniform_sampler-inl.h"
