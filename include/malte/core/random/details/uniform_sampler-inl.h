#include <malte/core/debug/assert.h>

namespace malte {

  template<typename T>
  inline T UniformSampler::next(const T min, const T max)
  {
    Assert(false);
    return static_cast<T>(0);
  }

  template<>
  inline f32 UniformSampler::next<f32>(const f32 min, const f32 max)
  {
    Assert(min <= max);
    std::uniform_real_distribution<f32> distribution(min, max);
    return distribution(m_random_generator);
  }

}  // malte
