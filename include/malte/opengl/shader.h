#pragma once

#include <string>

#include "opengl_headers.h"

#include <malte/core/interface/destroyable.h>

namespace malte {

  class GlShader final : public IDestroyable
  {
  public:
    enum class Type
    {
	    Vertex,
	    Fragment
    };

  public:
    GlShader(const Type type,
             const std::string& shader_name);

    ~GlShader();

    void destroy() override;

    GLuint get_handle() const;
    Type get_type() const;

  private:
    GLuint m_handle { 0 };
    Type m_type;

    std::string get_file_path(const Type type, const std::string& name);
    GLint get_shader_type(const Type type) const;

  };


}  // malte
