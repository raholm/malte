#pragma once

#include <string>

#include "opengl_headers.h"
#include "shader.h"

#include <malte/core/types.h>
#include <malte/core/math.h>
#include <malte/core/interface/non_copyable.h>

namespace malte {

  class GlShaderProgram : private INonCopyable
  {
  public:
    GlShaderProgram();

    GlShaderProgram(GlShaderProgram&& other);
    GlShaderProgram& operator=(GlShaderProgram&& rhs);

    ~GlShaderProgram();

    void attach(const GlShader& shader);
    void link();

    void use();

    void set(const std::string& name, const bool value);
    void set(const std::string& name, const f32 value);
    void set(const std::string& name, const s32 value);
    void set(const std::string& name, const u32 value);
    void set(const std::string& name, const glm::vec3& value);
    void set(const std::string& name, const glm::vec4& value);
    void set(const std::string& name, const glm::mat4& value);

  private:
    GLuint m_handle { 0 };

    GLint location(const std::string& name);

  };

}  // malte
