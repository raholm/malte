#version 460 core

uniform mat4 in_view;
uniform mat4 in_perspective;
uniform mat4 in_transform;
uniform vec4 in_color;

layout (location = 0) in vec3 in_position;

layout (location = 0) out vec4 out_color;

void main()
{
  gl_Position = in_perspective * in_view * in_transform * vec4(in_position, 1.0);
  out_color = in_color;
}
