#version 460 core

uniform mat4 in_view;
uniform mat4 in_perspective;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_world_position;
layout (location = 2) in vec3 in_world_velocity;

layout (location = 0) out vec4 out_color;

float DegreeToRadian(const float degree)
{
  return degree * 0.01745329;
}

mat4 RotateX(const float angle)
{
  const float cos_angle = cos(angle);
  const float sin_angle = sin(angle);

  mat4 result = mat4(1.0);
  result[1][1] = cos_angle;
  result[1][2] = -sin_angle;
  result[2][1] = sin_angle;
  result[2][2] = cos_angle;
  return result;
}

mat4 RotateY(const float angle)
{
  const float cos_angle = cos(angle);
  const float sin_angle = sin(angle);

  mat4 result = mat4(1.0);
  result[0][0] = cos_angle;
  result[0][2] = sin_angle;
  result[2][0] = -sin_angle;
  result[2][2] = cos_angle;
  return result;
}

mat4 RotateZ(const float angle)
{
  const float cos_angle = cos(angle);
  const float sin_angle = sin(angle);

  mat4 result = mat4(1.0);
  result[0][0] = cos_angle;
  result[0][1] = -sin_angle;
  result[1][0] = sin_angle;
  result[1][1] = cos_angle;
  return result;
}

mat4 Scale(const float scale)
{
  mat4 result = mat4(1.0);
  result[0][0] = scale;
  result[1][1] = scale;
  result[2][2] = scale;
  return result;
}

mat4 Translate(const vec3 translation)
{
  mat4 result = mat4(1.0);
  result[0][3] = translation.x;
  result[1][3] = translation.y;
  result[2][3] = translation.z;
  return transpose(result);
}

mat4 RotateAxis(const vec3 from, const vec3 to)
{
  const vec3 v = cross(from, to);
  const float sin_angle = length(v);
  const float cos_angle = dot(from, to);

  mat3 skew_symmetric_cross_product = mat3(0.0);
  skew_symmetric_cross_product[0][1] = -v.z;
  skew_symmetric_cross_product[0][2] = v.y;
  skew_symmetric_cross_product[1][0] = v.z;
  skew_symmetric_cross_product[1][2] = -v.x;
  skew_symmetric_cross_product[2][0] = -v.y;
  skew_symmetric_cross_product[2][1] = v.x;

  mat3 result = mat3(1.0);
  result +=
    skew_symmetric_cross_product +
    (1 - cos_angle) /
    (sin_angle * sin_angle) *
    skew_symmetric_cross_product *
    skew_symmetric_cross_product
    ;

  mat4 result2 = mat4(result);
  result2[3][3] = 1.0;
  return transpose(result2);
}

void main()
{
  const float scale = 10.0;

  const vec3 normalized_velocity = normalize(in_world_velocity);

  mat4 transform = mat4(1.0);
  transform *= Translate(in_world_position);
  transform *= RotateAxis(vec3(1.0, 0.0, 0.0), normalized_velocity);
  transform *= RotateY(DegreeToRadian(180.0));
  transform *= RotateX(DegreeToRadian(90.0));
  transform *= Scale(scale);

  gl_Position = in_perspective * in_view * transform * vec4(in_position, 1.0);
  // out_color = vec4(1.0, 1.0, 1.0, 1.0);
  out_color = vec4(normalized_velocity, 1.0);
}
