constant float close_distance = 50.0f;
constant float max_speed = 10.0f;

__local float3 LimitVelocity(const float3 own_velocity)
{
  const float speed = length(own_velocity);

  if (speed > max_speed)
    return max_speed * (own_velocity / speed);

  return own_velocity;
}

__local float3 LimitBounds(const float3 own_position)
{
  const float min_x = -960.0f;
  const float min_y = -540.0f;
  const float min_z = 0.0f;

  const float max_x = 960.0f;
  const float max_y = 540.0f;
  const float max_z = 320.0f;

  const float scale = 1.0f;

  float3 result = { 0.0f, 0.0f, 0.0f };

  if (own_position.x < min_x)
    result.x += scale;

  if (own_position.x > max_x)
    result.x -= scale;

  if (own_position.y < min_y)
    result.y += scale;

  if (own_position.y > max_y)
    result.y -= scale;

  if (own_position.z < min_z)
    result.z += scale;

  if (own_position.z > max_z)
    result.z -= scale;

  return result;
}

__local float3 CenterOfMassDirection(__global float* position_list,
                                     const float3 own_position,
                                     const unsigned int count)
{
  float3 result = { 0.0f, 0.0f, 0.0f };

  for (unsigned int index = 0; index < count; ++index)
    result += vload3(index, position_list);

  return (result - own_position) / (count - 1);
}

__local float3 SmallDistanceDirection(__global float* position_list,
                                      const float3 own_position,
                                      const unsigned int count)
{
  float3 result = { 0.0f, 0.0f, 0.0f };

  for (unsigned int index = 0; index < count; ++index)
  {
    const float3 position = vload3(index, position_list);
    const float3 delta_position = position - own_position;

    if (length(delta_position) >= close_distance) continue;

    result -= delta_position;
  }

  return result;
}

__local float3 PerceivedVelocity(__global float* velocity_list,
                                 const float3 own_velocity,
                                 const unsigned int count)
{
  float3 result = { 0.0f, 0.0f, 0.0f };

  for (unsigned int index = 0; index < count; ++index)
    result += vload3(index, velocity_list);

  return (result - own_velocity) / (count - 1);
}

__kernel
void UpdateBoid(__global float* position_list,
                __global float* velocity_list,
                const unsigned int count)
{
  int index = get_global_id(0);

  if (index >= count)
    return;

  float3 position = vload3(index, position_list);
  float3 velocity = vload3(index, velocity_list);

  const float3 rule1 = CenterOfMassDirection(position_list, position, count);
  const float3 rule2 = SmallDistanceDirection(position_list, position, count);
  const float3 rule3 = PerceivedVelocity(velocity_list, velocity, count);
  const float3 rule4 = LimitBounds(position);

  velocity += 0.0001f * rule1;
  velocity += 1.5f * rule2;
  velocity += 0.05f * rule3;
  velocity += 0.5f * rule4;
  velocity = LimitVelocity(velocity);
  position += velocity;

  barrier(CLK_GLOBAL_MEM_FENCE);

  vstore3(position, index, position_list);
  vstore3(velocity, index, velocity_list);
}
